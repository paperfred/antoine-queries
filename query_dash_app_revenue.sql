#table : souscritoo-1343:souscritoo_bi.product_app_ab_revenue_dash layer 7 
#views : souscritoo-1343:Views.product_app_ab_dash_1
#Dashboard : App dashboard
#standardSQL
SELECT 
  id_prospect  
, creation_date AS date
, DATE_TRUNC( creation_date ,WEEK) AS week  
, DATE_TRUNC( creation_date ,MONTH) AS month
, app_or_call_prospect 
, business_unit 
, source 
, IF(business_unit LIKE 'PPC' AND (source2 NOT LIKE 'bing' OR source2 IS NULL),'adwords',source2) AS source_lvl2      
, contracts_e
, is_client_e
, prob_revenue
, (id_app_user IS NOT NULL) AS full_app_or_not

FROM `souscritoo-1343.star_schema.attribution` 

LEFT JOIN 
(
  SELECT 
    id_prospect 
  , SUM(conversion_probability) AS contracts_e
  , MAX(conversion_probability) AS is_client_e
  , SUM(prob_revenue) AS prob_revenue
   
  FROM `souscritoo-1343.star_schema.fact_table_contract`  
  GROUP BY id_prospect
)
  USING(id_prospect)
  
WHERE 
  app_or_call_prospect LIKE 'app' 
  AND (business_unit NOT LIKE 'Miscellaneous' OR business_unit IS NULL)
  AND DATE_TRUNC(creation_date ,MONTH) >= "2017-10-01"