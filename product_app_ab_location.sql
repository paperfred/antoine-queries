# souscritoo-1343.Views.product_app_ab_location
WITH 
  table AS (
    SELECT DISTINCT 
      id_app_user 
    , id_prospect 
    , time 
    , COALESCE(
        IF(location IN ('box','energy','insurance','poste','mobile'), location, NULL)
      , CASE
          WHEN LOWER(location) LIKE '%nerg%' THEN 'energy'
          WHEN LOWER(location) LIKE '%box%' THEN 'box'
          WHEN LOWER(location) LIKE '%courrier%' THEN 'poste'
          WHEN LOWER(location) LIKE '%surance%' THEN 'insurance'
          WHEN LOWER(location) LIKE '%mobile%' THEN 'mobile'
        END
      , segment
      , CASE
          WHEN FIRST_VALUE(location) OVER sorted_contracts LIKE '%nerg%' THEN 'energy'
          WHEN FIRST_VALUE(location) OVER sorted_contracts LIKE '%box%' THEN 'box'
          WHEN FIRST_VALUE(location) OVER sorted_contracts LIKE '%courrier%' THEN 'poste'
          WHEN FIRST_VALUE(location) OVER sorted_contracts LIKE '%surance%' THEN 'insurance'
          WHEN FIRST_VALUE(location) OVER sorted_contracts LIKE '%mobile%' THEN 'mobile'
        END
      ) AS location

    FROM `souscritoo-1343.Views.product_fm_app_actions` 

    LEFT JOIN  (
      SELECT 
        id_app_user 
      , id_prospect 
      , IF(segment IN ('energy','box','insurance'), segment, NULL) AS segment
      FROM `souscritoo-1343.star_schema.attribution`
    ) USING(id_app_user)
  
    WINDOW sorted_contracts AS (
      PARTITION BY id_app_user 
      ORDER BY event != 'validated_contract', date, time
    )
  )

, FIRST_LOC AS (
  SELECT DISTINCT 
    id_app_user
  , id_prospect 
  , FIRST_VALUE(location IGNORE NULLS) OVER sorted AS first_location

  FROM table 

  WINDOW
    sorted AS (
      PARTITION BY id_app_user 
      ORDER BY time 
      ROWS BETWEEN 
        UNBOUNDED PRECEDING 
        AND UNBOUNDED FOLLOWING
    )
  )

, LOC AS (
    SELECT DISTINCT 
      id_app_user
    , id_prospect 
    , location 
    FROM table 
  )

, NB_LOC AS (
    SELECT DISTINCT 
      id_app_user
    , id_prospect 
    , COUNT(IF(location IS NULL,'_',location)) AS nb_location 
    FROM LOC
    GROUP BY id_app_user, id_prospect  
  )

SELECT DISTINCT 
  id_app_user
, id_prospect 
, location 
, first_location

FROM LOC

JOIN FIRST_LOC
  USING(id_app_user,id_prospect)

JOIN NB_LOC
  USING(id_app_user,id_prospect)

WHERE location IS NOT NULL 
  OR (location IS NULL AND nb_location = 1)