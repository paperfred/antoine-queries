#Initialisation_R
SELECT 
  DATE_TRUNC( creation_date ,MONTH) AS month 
, business_unit
, COUNTIF(is_client)/COUNT(*)

FROM `star_schema.attribution`
LEFT JOIN
  (
    SELECT
      id_prospect
    , MAX(contract_status = 'BRUT') AS is_client
      
    FROM `star_schema.fact_table_contract` 
    
    GROUP BY id_prospect
  )
  USING(id_prospect)
WHERE status_inapp IS NOT NULL

GROUP BY month, business_unit 
ORDER BY month DESC, business_unit 
