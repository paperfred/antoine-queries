#Is app prospect 
SELECT 
  DATE_TRUNC(creation_date, MONTH) AS month
, business_unit 
, COUNT(*) AS app_prospects
  
FROM `star_schema.attribution` 
WHERE id_app_user IS NOT NULL
GROUP BY month, business_unit
ORDER BY month DESC, business_unit