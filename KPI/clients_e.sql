#Clients_e
SELECT 
  business_unit
, DATE_TRUNC(creation_date, MONTH) AS month
, SUM(is_client_e) AS clients_e

FROM `star_schema.attribution`

LEFT JOIN
  (
    SELECT
      id_prospect
    , MAX(conversion_probability) AS is_client_e
    
    FROM `star_schema.fact_table_contract` 
    
    GROUP BY id_prospect
  )
  USING (id_prospect)

WHERE status_inapp LIKE 'late_churn' 

GROUP BY month, business_unit
ORDER BY month DESC, business_unit
