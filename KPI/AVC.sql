#App Validated Rate

SELECT 
  DATE_TRUNC( creation_date ,MONTH) AS month 
, business_unit
, COUNTIF( id_app_user IS NOT NULL )/COUNT(*)
FROM `star_schema.attribution` 
GROUP BY month, business_unit 
ORDER BY month DESC, business_unit 