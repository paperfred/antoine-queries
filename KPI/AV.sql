#App Validated

SELECT 
  DATE_TRUNC( creation_date ,MONTH) AS month 
, business_unit
, COUNTIF( id_app_user IS NOT NULL )
FROM `star_schema.attribution` 
WHERE status_inapp LIKE 'validated'
GROUP BY month, business_unit 
ORDER BY month DESC, business_unit 