#Rclient
SELECT 
  business_unit
, DATE_TRUNC(creation_date, MONTH) AS month
, COUNTIF(is_rawClient) AS raw_clients

FROM `star_schema.attribution`

LEFT JOIN
  (
    SELECT
      id_prospect
    , MAX(contract_status IN ('NET','BRUT')) AS is_rawClient
      
    FROM `star_schema.fact_table_contract` 
    
    GROUP BY id_prospect
  )
  USING (id_prospect)

WHERE status_inapp LIKE 'late_churn'

GROUP BY month, business_unit
ORDER BY month DESC, business_unit