#AV
SELECT 
  DATE_TRUNC( creation_date ,MONTH) AS month 
  , business_unit
  ,COUNT( id_prospect ) AS nb_AV 
FROM `star_schema.attribution` 
WHERE status_inapp LIKE 'validated'
GROUP BY month , business_unit
ORDER BY month DESC , business_unit