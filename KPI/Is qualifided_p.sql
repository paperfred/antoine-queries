    SELECT 
    COUNT(DISTINCT IF(IFNULL(reached_offers OR validated_contract_app, FALSE),id_prospect, NULL)) AS qualifided_p
    , DATE_TRUNC(DATE(date_joined),MONTH) AS month
    , business_unit

    FROM #MASTER WINDOW
      (
        SELECT 
          CAST(id_app_user AS STRING) AS id_app_user
        , date_joined
        FROM `souscritoo-1343.star_schema.dimension_app_user`
      )

    LEFT JOIN 
      (
        SELECT 
          CAST(id_app_user AS STRING) AS id_app_user
        , MAX(valid) AS validated_contract_app
        FROM `souscritoo-1343.star_schema.fact_table_app_contract`
        GROUP BY id_app_user       
      )
      USING (id_app_user)

    FULL JOIN
      (
        SELECT
          id_universal AS id_app_user
        , MAX(REGEXP_CONTAINS(provider,'[[:alpha:]]')) AS reached_offers
        FROM `souscritoo-1343.star_schema.fact_table_tracking_view_price`
        GROUP BY id_universal 
      ) AS view_price
        USING (id_app_user)

    JOIN `star_schema.prospect` 
      USING (id_app_user) 
    JOIN `star_schema.dimension_business_unit` 
      ON id_bu = id_dim_business_unit

    GROUP BY  business_unit, month 
    ORDER BY  month DESC, business_unit