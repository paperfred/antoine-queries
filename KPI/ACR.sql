#ACR 

SELECT 
  DATE_TRUNC( creation_date ,MONTH) AS month 
, business_unit
, COUNTIF(status_inapp LIKE 'validated' )/COUNT(*)
FROM `star_schema.attribution` 

WHERE id_app_user IS NOT NULL

GROUP BY month, business_unit 
ORDER BY month DESC, business_unit 