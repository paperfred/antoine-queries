# latechurn_propspect 
SELECT DATE_TRUNC( creation_date , MONTH) AS month
	, business_unit
	, COUNT(DISTINCT id_prospect ) nb_latechurn
FROM `star_schema.attribution` 
WHERE status_inapp LIKE 'late_churn'
GROUP BY month , business_unit
ORDER BY month DESC , business_unit