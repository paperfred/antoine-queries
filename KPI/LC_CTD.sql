#LC_CTD

SELECT 
    DATE_TRUNC( creation_date ,MONTH) AS month 
  , business_unit
  , COUNT(ctd)/COUNT(*)
FROM `star_schema.attribution`
LEFT JOIN 
(
  SELECT id_prospect 
  , eloquant_campaign  LIKE 'C_App_Latechurn' AS ctd
  FROM `star_schema.fact_table_call` 
  JOIN `star_schema.dimension_eloquant` 
    ON id_dim_eloquant = id_call
  WHERE eloquant_campaign  LIKE 'C_App_Latechurn'
)
USING (id_prospect)   

WHERE status_inapp LIKE 'late_churn'

GROUP BY month, business_unit 
ORDER BY month DESC, business_unit