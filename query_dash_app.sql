#standardSQL
#Table name: "souscritoo-1343:souscritoo_bi.product_app_ab_main_dash"
#Layer: "pipeline_layer_6"
#Dashboard : App dashboard
WITH
  dim_app_user AS (
  # User data, 1 row per user
    SELECT
      CAST(id_app_user AS STRING) AS id_app_user
    , ( user_email IS NOT NULL
        AND user_phone IS NOT NULL
        AND id_address IS NOT NULL
        AND user_first_name IS NOT NULL
        AND user_last_name IS NOT NULL
      ) AS filled_personal_info
    , id_text
    , DATE(date_joined) AS date
    , origin_user

    FROM `souscritoo-1343.star_schema.dimension_app_user`
  )

, app_contracts AS (
    # Summary of contracts and revenue, 1 row per user & type of contract (aka location)
    SELECT
      CAST(id_app_user AS STRING) AS id_app_user

    , CASE
        WHEN id_dim_app_contract_box IS NOT NULL THEN 'box'
        WHEN id_dim_app_contract_energy IS NOT NULL THEN 'energy'
        WHEN id_dim_app_contract_insurance IS NOT NULL THEN 'insurance'
        WHEN id_dim_app_contract_poste IS NOT NULL THEN 'poste'
        WHEN id_dim_app_contract_cellular IS NOT NULL THEN 'mobile'
      END AS location

    , MAX((origin_contract = 'app' OR f.id_dim_created_date < 20180822) AND valid) AS valid
    , MAX(contract_status = 'NET') AS is_client
    , MAX(conversion_probability) AS is_client_e
    , SUM(conversion_probability) AS contracts_e
    , SUM(net_revenue) AS net_revenue
    , SUM(prob_revenue) AS prob_revenue

    FROM `souscritoo-1343.star_schema.fact_table_app_contract`  AS f

    LEFT JOIN `souscritoo-1343.star_schema.fact_table_contract`
      USING(id_prospect,id_dim_sf_opportunity)

    GROUP BY id_app_user, location
  )

, payment AS (
    # Selected payment method, 1 row per user
    SELECT
      CAST(id_app_user AS STRING) AS id_app_user
    , MAX(user_iban IS NOT NULL OR  user_payment_method = TRUE ) AS filled_payment_info

    FROM `souscritoo-1343.star_schema.dimension_app_user`

    LEFT JOIN (
      SELECT
        id_app_user
      , MAX(
          LOWER(user_payment_method) NOT LIKE 'iban'
        ) AS user_payment_method
      FROM `souscritoo-1343.star_schema.fact_table_app_contract`
      LEFT JOIN `souscritoo-1343.star_schema.dimension_app_contract_box`
        USING (id_dim_app_contract_box)
      LEFT JOIN `souscritoo-1343.star_schema.dimension_app_contract_energy`
        USING (id_dim_app_contract_energy,user_payment_method )
      LEFT JOIN `souscritoo-1343.star_schema.dimension_app_contract_insurance`
        USING (id_dim_app_contract_insurance,user_payment_method)
      LEFT JOIN `souscritoo-1343.star_schema.dimension_app_contract_poste`
        USING (id_dim_app_contract_poste,user_payment_method )
      GROUP BY id_app_user
    ) USING(id_app_user)

    GROUP BY id_app_user
  )

, final_table AS (
    SELECT
      # 1 row per user & location
      id_app_user
    , location
    , attr.id_prospect
    , dim_app_user.date
    , DATE_TRUNC(dim_app_user.date, WEEK) AS week
    , DATE_TRUNC(dim_app_user.date, MONTH) AS month
    , IF(dim_app_user.filled_personal_info = TRUE OR app_contracts.valid = TRUE, id_app_user, NULL) AS filled_personal_info
    , IF(attr.is_qualifiedProspect, id_app_user, NULL) AS is_qualifided_p
    , IF(app_contracts.valid = TRUE, id_app_user, NULL) AS is_validated_p
    , IF(payment.filled_payment_info = TRUE, id_app_user, NULL) AS filled_payment_info
    , IF(is_client = TRUE OR app_contracts.is_client_e = 1.0, id_app_user, NULL) AS is_client
    , IF(location = first_location, MAX(app_contracts.is_client_e) OVER (PARTITION BY id_prospect), NULL) AS is_client_e_no_location
    , app_contracts.is_client_e
    , app_contracts.net_revenue
    , app_contracts.prob_revenue
    , dim_app_user.id_text
    , IFNULL(dim_app_user.origin_user, attr.app_or_call_prospect) AS app_or_call_prospect
    , attr.status_inapp
    , attr.business_unit
    , attr.source AS source_lvl1
    , IF(attr.source = 'paid_acquisition' AND IFNULL(attr.source2= 'google', TRUE),'adwords', attr.source2) AS source_lvl2
    , IFNULL(device, 'unknown') AS device
    , IFNULL(browser, 'unknown') AS browser

    FROM dim_app_user

    JOIN `souscritoo-1343.star_schema.attribution` attr
      USING (id_app_user)

    LEFT JOIN (
      # Location   
      SELECT * EXCEPT (id_prospect)
      FROM `souscritoo-1343.Views.product_app_ab_location`
    ) AS locations USING (id_app_user)

    LEFT JOIN app_contracts 
      USING (id_app_user, location)

    LEFT JOIN payment 
      USING (id_app_user)

    WHERE date >= '2017-10-01'
  )

SELECT
  *
FROM final_table