#Views : souscritoo-1343:Views.product_app_ab_AVG_time
#Dashboard : App dashboard
WITH 
  A AS 
    (
    SELECT DISTINCT 
      FIRST_VALUE(timestamp) OVER (PARTITION BY user_id ORDER BY timestamp) AS timestamp_validation  
    , user_id AS id_app_user 
    , id_text 
    FROM `souscritoo-1343.prod_app_sct.validated_contract` 
    JOIN `souscritoo-1343.star_schema.dimension_app_user` 
      ON CAST(id_app_user AS STRING) = user_id 
    WHERE REGEXP_CONTAINS(location ,r'energy|energie')
    ORDER BY timestamp_validation  
    )
, 
  B AS 
    (
    SELECT DISTINCT 
      user_id AS id_app_user 
    , name AS energy_type
    , DATE_TRUNC(DATE(FIRST_VALUE(timestamp) OVER (PARTITION BY user_id, name ORDER BY timestamp)),WEEK) AS date 
    , FIRST_VALUE(timestamp) OVER (PARTITION BY user_id, name ORDER BY timestamp) AS timestamp_energy_type  
    FROM `souscritoo-1343.prod_app_sct.pages` 
    WHERE 
      REGEXP_CONTAINS(context_page_path,r'(/onboarding|/mon-compte/energie/\d{0,2})') 
      AND DATE(timestamp) >= '2018-06-23' 
      AND user_id IS NOT NULL
      AND name = 'is_moving'
    ORDER BY id_app_user
    )
  
SELECT 
    id_app_user 
  , TIMESTAMP_DIFF(timestamp_validation,timestamp_energy_type, SECOND) AS time_diff
  , date
  , id_text 
  , device 
  FROM A
  LEFT JOIN B 
    USING(id_app_user)
  LEFT JOIN `souscritoo-1343.Views.product_app_ab_device`
    USING(id_app_user)
  WHERE energy_type IS NOT NULL 
  GROUP BY id_app_user, time_diff, date, id_text, device    
  HAVING time_diff > 0
  ORDER BY date,id_app_user