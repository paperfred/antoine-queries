#souscritoo-1343:Views.product_app_dash_de_controle
#Dashboard : Dashboard v3
SELECT 
  id_app_user 
, id_text 
, is_moving 
, date_joined 
, app_or_call_user 
, device 
, source
, source2
, IFNULL(energie_start,0) AS energie_start
, IFNULL(box_start,0) AS box_start
, IFNULL(assurance_start,0) AS assurance_start
, IFNULL(mobile_start,0) AS mobile_start
, IFNULL(poste_start,0) AS poste_start
, IFNULL(resiliation_energie_start,0) AS resiliation_energie_start
, IFNULL(resiliation_box_start,0) AS resiliation_box_start
, IFNULL(resiliation_mobile_start,0) AS resiliation_mobile_start
, IFNULL(resiliation_assurance_start,0) AS resiliation_assurance_start
, IFNULL( valid_box_app ,0) AS valid_box_app
, IFNULL( valid_energy_app ,0) AS valid_energy_app
, IFNULL( valid_insurance_app ,0) AS valid_insurance_app
, IFNULL( valid_poste_app ,0) AS valid_poste_app
, IFNULL( valid_cellular_app ,0) AS valid_cellular_app
, IFNULL( valid_box_call ,0) AS valid_box_call
, IFNULL( valid_energy_call ,0) AS valid_energy_call
, IFNULL( valid_insurance_call ,0) AS valid_insurance_call
, IFNULL( valid_poste_call ,0) AS valid_poste_call
, IFNULL( valid_cellular_call ,0) AS valid_cellular_call
, IFNULL( resiliation_assurance_valid ,0) AS resiliation_assurance_valid
, IFNULL( resiliation_box_valid ,0) AS resiliation_box_valid
, IFNULL( resiliation_energie_valid ,0) AS resiliation_energie_valid
, IFNULL( resiliation_mobile_valid ,0) AS resiliation_mobile_valid

FROM # dim_app_user
  (
  SELECT
    CAST(id_app_user AS STRING) AS id_app_user   
  , id_text 
  , is_moving 
  , DATE(date_joined) AS date_joined 
  , DATE(IFNULL(last_login ,date_joined)) AS last_login 
  , origin_user AS app_or_call_user 
  FROM `souscritoo-1343.star_schema.dimension_app_user`
  )
  
LEFT JOIN 
  (
  SELECT
    id_app_user 
  , id_prospect   
  , source
  , source2
  
  FROM `souscritoo-1343.star_schema.attribution` 
  )
  USING(id_app_user)

LEFT JOIN 
  (
  SELECT 
    id_universal AS id_app_user
  , CAST(MAX(IFNULL(REGEXP_CONTAINS(context_page_path,r'energie'),FALSE)) AS INT64) AS energie_start 
  , CAST(MAX(IFNULL(REGEXP_CONTAINS(context_page_path,r'box'),FALSE)) AS INT64) AS box_start 
  , CAST(MAX(IFNULL(REGEXP_CONTAINS(context_page_path,r'assurance'),FALSE)) AS INT64) AS assurance_start 
  , CAST(MAX(IFNULL(REGEXP_CONTAINS(context_page_path,r'mobile'),FALSE)) AS INT64) AS mobile_start 
  , CAST(MAX(IFNULL(REGEXP_CONTAINS(context_page_path,r'courrier'),FALSE)) AS INT64) AS poste_start 
  , CAST(MAX(IFNULL(REGEXP_CONTAINS(context_page_path,r'resiliation-energie'),FALSE)) AS INT64) AS resiliation_energie_start 
  , CAST(MAX(IFNULL(REGEXP_CONTAINS(context_page_path,r'resiliation-box'),FALSE)) AS INT64) AS resiliation_box_start
  , CAST(MAX(IFNULL(REGEXP_CONTAINS(context_page_path,r'resiliation-mobile'),FALSE)) AS INT64) AS resiliation_mobile_start 
  , CAST(MAX(IFNULL(REGEXP_CONTAINS(context_page_path,r'resiliation-assurance'),FALSE)) AS INT64) AS resiliation_assurance_start  
  FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 
  GROUP BY 1
  )
  USING(id_app_user)

LEFT JOIN 
 (
 SELECT 
   CAST(id_app_user AS STRING ) AS id_app_user 
 ,  MAX(IF(id_dim_app_contract_box IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'app'),1,0)) AS valid_box_app
 ,  MAX(IF(id_dim_app_contract_energy IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'app'),1,0)) AS valid_energy_app
 ,  MAX(IF(id_dim_app_contract_insurance IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'app'),1,0))AS valid_insurance_app
 ,  MAX(IF(id_dim_app_contract_poste IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'app'),1,0)) AS valid_poste_app
 ,  MAX(IF(id_dim_app_contract_cellular IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'app'),1,0))AS valid_cellular_app
 ,  MAX(IF(id_dim_app_contract_box IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'call'),1,0)) AS valid_box_call
 ,  MAX(IF(id_dim_app_contract_energy IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'call'),1,0)) AS valid_energy_call
 ,  MAX(IF(id_dim_app_contract_insurance IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'call'),1,0)) AS valid_insurance_call
 ,  MAX(IF(id_dim_app_contract_poste IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'call'),1,0)) AS valid_poste_call
 ,  MAX(IF(id_dim_app_contract_cellular IS NOT NULL AND valid AND IF(id_dim_created_date < 20180819, TRUE, origin_contract = 'call'),1,0)) AS valid_cellular_call
 
 FROM `souscritoo-1343.star_schema.fact_table_app_contract` 
 GROUP BY 1 
 ORDER BY id_app_user 
 )
 USING(id_app_user)
 
 LEFT JOIN 
 (
 SELECT 
   id_prospect 
 , SUM(CAST(status = 'CANCEL_VALID' AS INT64)) AS sum_cancellations 
 FROM `souscritoo-1343.star_schema.fact_table_cancellations` 
 WHERE cancelled_from_app 
 GROUP BY 1
 )
 USING(id_prospect)
 
 LEFT JOIN 
 (
  SELECT 
    id_prospect   
  , MAX(IF(REGEXP_CONTAINS(recordtype_name,'MRH') AND status = 'CANCEL_VALID',1,0)) AS resiliation_assurance_valid
  , MAX(IF(REGEXP_CONTAINS(recordtype_name,'Box') AND status = 'CANCEL_VALID',1,0)) AS resiliation_box_valid
  , MAX(IF(REGEXP_CONTAINS(recordtype_name,'Energie') AND status = 'CANCEL_VALID',1,0)) AS resiliation_energie_valid
  , MAX(IF(REGEXP_CONTAINS(recordtype_name,'Mobile') AND status = 'CANCEL_VALID',1,0)) AS resiliation_mobile_valid
  FROM `souscritoo-1343.star_schema.fact_table_cancellations` 
  WHERE cancelled_from_app 
  GROUP BY 1
  ORDER BY id_prospect 
 )
 USING(id_prospect)
 
LEFT JOIN `souscritoo-1343.Views.product_app_ab_device` 
  USING(id_app_user) 
  
WHERE date_joined >= '2018-07-30' #date new dash 
