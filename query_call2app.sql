#souscritoo-1343:Views.product_app_ab_call2app_2
WITH 
 call2app_user AS 
    ( 
    SELECT 
      COUNT(DISTINCT id_app_user) AS app_user
    , DATE(date_joined) AS date
    , DATE_TRUNC( DATE(date_joined) , WEEK) AS week
    , DATE_TRUNC( DATE(date_joined) , MONTH) AS month
    , email_or_pro
    FROM `souscritoo-1343.star_schema.dimension_app_user` 
    LEFT JOIN 
      (
      SELECT DISTINCT 
        id_anonymous 
      , id_universal  
      , CASE 
         WHEN referrer LIKE '%pro.papernest.com%' THEN 'pro_papernest'
         WHEN NOT(REGEXP_CONTAINS(referrer,'pro.papernest.com')) THEN 'email'
        END as email_or_pro 
      FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 
      WHERE url LIKE '%call2app%'
      )
      ON CAST(id_app_user AS STRING) = id_universal 
    WHERE origin_user LIKE 'call'
    GROUP BY 2,3,4,5
    ORDER BY 2 DESC 
    )
,
  call2app_contracts AS 
    (
    SELECT 
      date_actual AS date 
    , DATE_TRUNC(date_actual, WEEK) AS week
    , DATE_TRUNC(date_actual, MONTH) AS month
    , email_or_pro
    , SUM(IF(valid,1,0)) AS sum_contracts 
    , COUNT(DISTINCT IF(valid,id_app_user,NULL)) AS client_valid  
    , SUM(contracts_e) AS contracts_e 
    , SUM(is_client_e) AS is_client_e 
    FROM 
   (
    SELECT 
      CAST(id_app_user AS STRING) AS id_app_user 
    , id_dim_app_contract_box
    , id_dim_app_contract_energy
    , id_dim_app_contract_insurance
    , id_dim_app_contract_poste
    , id_dim_app_contract_cellular
    , valid 
    , id_dim_created_date 
    , origin_contract 
    FROM `souscritoo-1343.star_schema.fact_table_app_contract` 
    JOIN (SELECT id_app_user FROM `souscritoo-1343.star_schema.dimension_app_user` WHERE origin_user = 'call')
      USING(id_app_user)
    )
    LEFT JOIN `souscritoo-1343.star_schema.dimension_date` 
      ON id_dim_created_date = id_dim_date
    LEFT JOIN `souscritoo-1343.star_schema.attribution` 
      USING(id_app_user)
    LEFT JOIN 
    (
    SELECT 
      id_prospect 
    , SUM(conversion_probability) AS contracts_e 
    , MAX(conversion_probability ) AS is_client_e
    FROM `souscritoo-1343.star_schema.fact_table_contract`
    GROUP BY 1
    )
    USING(id_prospect)
    LEFT JOIN 
    (
    SELECT DISTINCT 
      id_anonymous 
    , id_universal AS id_app_user  
    , CASE 
         WHEN referrer LIKE '%pro.papernest.com%' THEN 'pro_papernest'
         WHEN NOT(REGEXP_CONTAINS(referrer,'pro.papernest.com')) THEN 'email'
       END as email_or_pro 
    FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 
    WHERE url LIKE '%call2app%'
    )
    USING(id_app_user)
    
    WHERE origin_contract LIKE 'app'
    GROUP BY 1,2,3,4
    ORDER BY date DESC 
    )
,
 call2app_mdp AS 
    (
      SELECT DISTINCT 
        COUNT(DISTINCT id_anonymous) AS user_mdp  
      , date_actual AS date 
      , DATE_TRUNC( date_actual , WEEK) AS week
      , DATE_TRUNC( date_actual , MONTH) AS month
      , CASE 
         WHEN referrer LIKE '%pro.papernest.com%' THEN 'pro_papernest'
         WHEN NOT(REGEXP_CONTAINS(referrer,'pro.papernest.com')) THEN 'email'
        END as email_or_pro
      FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 
      JOIN `souscritoo-1343.star_schema.dimension_date` 
        USING(id_dim_date )
      WHERE url LIKE '%call2app%'
      GROUP BY 2,3,4,5
    )

SELECT *
FROM call2app_user 
LEFT JOIN call2app_contracts 
  USING(date,week,month,email_or_pro)
LEFT JOIN call2app_mdp
  USING(date,week,month,email_or_pro)
ORDER BY date DESC 