#Views : souscritoo-1343:Views.product_app_ab_SEA_dash_2
#Dashboard : App dashboard
WITH click AS
  (
    SELECT 
      DATE_TRUNC(date_actual, WEEK) AS week 
    , COUNT(IF(app_or_call_campaign LIKE 'app',gclid, NULL)) AS gclid_app 
    FROM  `souscritoo-1343.souscritoo_bi.bi_gclid_report`  
    GROUP BY 1
  )
,

prospect AS 
  (
    SELECT 
      COUNT(IF(REGEXP_CONTAINS(funnel_type , r'^\w+_webapp')= TRUE,id_prospect,NULL)) AS prospect_app
    , COUNTIF(IF(REGEXP_CONTAINS(funnel_type , r'^\w+_webapp')= TRUE,is_qualifiedProspect,NULL)) AS is_qualifiedProspect_app
    , COUNTIF(IF(REGEXP_CONTAINS(funnel_type , r'^\w+_webapp')= TRUE,is_raw_client,NULL)) AS is_raw_client_app 
    , SUM(IF(REGEXP_CONTAINS(funnel_type , r'^\w+_webapp')= TRUE,is_client,NULL)) AS is_client_app 
    , COUNTIF(validated) AS app_validated
    , DATE_TRUNC(DATE(joined_at),WEEK)  AS week
    FROM `souscritoo-1343.star_schema.prospect_source_v2` 
    LEFT JOIN 
        (
          SELECT
            id_prospect 
          , MAX(IFNULL(contract_status IN ('NET','BRUT'),FALSE)) AS is_raw_client
          , MAX(conversion_probability) AS is_client
          FROM `souscritoo-1343.star_schema.fact_table_contract` 
          GROUP BY id_prospect 
        )
        USING(id_prospect )
    LEFT JOIN
        (
        SELECT 
          id_prospect 
        , MAX(valid) AS validated
        FROM `souscritoo-1343.star_schema.fact_table_app_contract` 
        GROUP BY id_prospect 
        )
        USING(id_prospect )   
    WHERE source_lvl1 = 'paid_acquisition'
    AND IFNULL(source_lvl2 = 'google', TRUE)
    
    GROUP BY week 
  )
  
SELECT * 
FROM click 
JOIN prospect 
  USING(week) 
ORDER BY week DESC