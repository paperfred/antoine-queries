#souscritoo-1343:Views.product_ab_test_cancellation_v1
#Dashboard : Funnel App Only
WITH 
  table AS (
    SELECT 
      date 
    , id_text
    , device 
    , id_app_user 
    , CAST(provider_and_date AS INT64) AS provider_and_date
    , CAST(page_converted_provider_and_date AS INT64) AS page_converted_provider_and_date
    , CAST(address AS INT64) AS address
    , CAST(page_converted_address AS INT64) AS page_converted_address
    , CAST(reference_contract AS INT64) AS reference_contract
    , CAST(page_converted_reference_contract AS INT64) AS page_converted_reference_contract
    , CAST(elec_meter AS INT64) AS elec_meter
    , CAST(page_converted_elec_meter AS INT64) AS page_converted_elec_meter
    , CAST(gas_meter AS INT64) AS gas_meter
    , CAST(page_converted_gas_meter AS INT64) AS page_converted_gas_meter
    , CAST(summary AS INT64) AS summary
    , CAST(page_converted_summary AS INT64) AS page_converted_summary
    , CAST(resiliation_energie_valid AS INT64) AS resiliation_energie_valid

    # dim_app_user
    FROM (
      SELECT 
        CAST(id_app_user AS STRING) AS id_app_user
      , id_text
      , DATE(date_joined) AS date
      FROM `souscritoo-1343.star_schema.dimension_app_user`
    )

    # pages    
    JOIN (
      SELECT
        id_universal AS id_app_user
      , ANY_VALUE(context_user_agent) AS context_user_agent
      , MAX(name = 'provider_and_date') AS provider_and_date
      , MAX(name = 'page_converted_provider_and_date') AS page_converted_provider_and_date
      , MAX(name = 'address') AS address
      , MAX(name = 'page_converted_address') AS page_converted_address
      , MAX(name = 'reference_contract') AS reference_contract
      , MAX(name = 'page_converted_reference_contract') AS page_converted_reference_contract
      , MAX(name = 'elec_meter') AS elec_meter
      , MAX(name = 'page_converted_elec_meter') AS page_converted_elec_meter
      , MAX(name = 'gas_meter') AS gas_meter
      , MAX(name = 'page_converted_gas_meter') AS page_converted_gas_meter
      , MAX(name = 'summary') AS summary
      , MAX(name = 'page_converted_summary') AS page_converted_summary

      FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 

      WHERE url LIKE '%mon-compte/resiliation-energie%'

      GROUP BY id_app_user
    ) USING (id_app_user)

    LEFT JOIN (
      SELECT 
        id_app_user 
      , id_prospect 
      FROM `souscritoo-1343.star_schema.attribution` 
    ) USING(id_app_user) 

    # Validation résiliation
    LEFT JOIN (
      SELECT 
        id_prospect 
      , MAX(status IN (
          'CANCEL_VALID',
          'CANCEL_INQUIRY',
          'CANCEL_ABORT',
          'CANCEL_MAIL_SENT',
          'CANCEL_RECEIPT_RECEIVED'
        )) AS resiliation_energie_valid
      FROM `souscritoo-1343.star_schema.fact_table_cancellations` 
      WHERE cancelled_from_app 
        AND recordtype_name = 'Résiliation Energie'
      GROUP BY id_prospect
    ) USING (id_prospect)
    
    # device and browser 
    LEFT JOIN (
      SELECT 
        context_user_agent
      , ANY_VALUE(browser) AS browser
      , ANY_VALUE(browser_version) AS browser_version
      , ANY_VALUE(os) AS os
      , ANY_VALUE(os_version) AS os_version

      , CASE
          WHEN ANY_VALUE(pc) THEN 'desktop'
          WHEN ANY_VALUE(tablet) THEN 'tablet'
          WHEN ANY_VALUE(mobile) THEN 'mobile'
        END AS device
        
      , ANY_VALUE(device_brand) AS device_brand
      , ANY_VALUE(device_model) AS device_model

      FROM `souscritoo-1343.Views.product_ar_useragent` 

      GROUP BY context_user_agent
    ) USING (context_user_agent)
  )

, table2 AS (
    SELECT 
      date 
    , id_text
    , device 
    , id_app_user
    , provider_and_date 
    , address 
    , reference_contract 
    , elec_meter 
    , gas_meter 
    , summary 
    ,	IF(date > '2018-08-24',page_converted_summary,IF(resiliation_energie_valid=1,summary,0)) AS summary_conso
    , IF(resiliation_energie_valid=1,1,page_converted_summary) AS resiliation_energie_valid
  	, IF(date > '2018-08-24',page_converted_provider_and_date,IF((summary + elec_meter + gas_meter + reference_contract + address)>=1 ,provider_and_date,0)) AS provider_and_date_conso
    , IF(date > '2018-08-24',page_converted_address,IF((summary + elec_meter + gas_meter + reference_contract )>=1 ,address,0)) AS address_conso
    , IF(date > '2018-08-24',page_converted_reference_contract,IF((summary + elec_meter + gas_meter )>=1 ,reference_contract,0)) AS reference_contract_conso
    , IF(date > '2018-08-24',page_converted_gas_meter,IF((summary + elec_meter)>=1 ,gas_meter,0)) AS gas_meter_conso
    , IF(date > '2018-08-24',page_converted_elec_meter,IF((summary)>=1 ,elec_meter,0)) AS elec_meter_conso
    FROM table 
  )
    
SELECT 
  date 
, id_text 
, device 
, COUNT(DISTINCT id_app_user) AS appuser 
, SUM(summary_conso) AS summary_conso 
, SUM(resiliation_energie_valid) AS resiliation_energie_valid
, SUM(summary) AS summary
, SUM(elec_meter_conso) AS elec_meter_conso
, SUM(elec_meter) AS elec_meter 
, SUM(gas_meter) AS gas_meter 
, SUM(gas_meter_conso) AS gas_meter_conso
, SUM(reference_contract ) AS reference_contract 
, SUM(reference_contract_conso ) AS reference_contract_conso
, SUM(address) AS address 
, SUM(address_conso) AS address_conso
, SUM(provider_and_date) AS provider_and_date 
, SUM(provider_and_date_conso) AS provider_and_date_conso  
FROM table2
GROUP BY date, id_text, device