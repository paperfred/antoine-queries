#souscritoo-1343:Views.product_ab_test_poste
#Dashboard : Funnel App Only
WITH 
  table AS (
    SELECT 
      id_app_user 
    , date 
    , id_text 
    , device 
    , source
    , source2
    , client_e
    # consolidation avec les pages converted
    , IF(date_moving = 1,1,page_converted_date_moving) AS date_moving
    , IF(address = 1,1,page_converted_address) AS address
    , IF(offers = 1,1,page_converted_offers) AS offers 
    , IF(personal_info = 1,1,page_converted_personal_info) AS personal_info
    , IF(summary = 1,1,page_converted_summary) AS summary
    , IF(code_activation = 1, 1, page_converted_code_activation) AS code_activation
    , IF(payment = 1, 1, page_converted_payment) AS payment
    
    # consolidation avant les page_converted 
    , IF(date > '2018-08-24',page_converted_date_moving,IF((personal_info+offers+address+summary)>=1,date_moving,0)) AS date_moving_conso
    , IF(date > '2018-08-24',page_converted_address,IF((personal_info+offers+summary)>=1,address,0)) AS address_conso
    , IF(date > '2018-08-24',page_converted_offers,IF((personal_info+summary)>=1,offers,0)) AS offers_conso
    , IF(date > '2018-08-24',page_converted_personal_info,IF((summary)>=1,personal_info,0)) AS personal_info_conso
    , page_converted_code_activation AS code_activation_conso
    , page_converted_payment AS payment_conso
    , IF(date > '2018-08-24',page_converted_summary,IF(valid = TRUE,summary,0)) AS summary_conso 
    , IF(date < '2018-08-19',IF(valid = TRUE,summary,0),CAST(IFNULL(valid,FALSE) AS INT64)) AS valid 

    # dim_app_user
    FROM (
      SELECT
        CAST(id_app_user AS STRING) AS id_app_user   
      , id_text 
      , DATE(date_joined) AS date 
      FROM `souscritoo-1343.star_schema.dimension_app_user`
      WHERE DATE(date_joined) >= '2018-07-18'
    )

    # pages
    JOIN (
      SELECT 
        id_universal AS id_app_user
      , ANY_VALUE(context_user_agent) AS context_user_agent
      , CAST(MAX(name = 'date_moving') AS INT64) AS date_moving
      , CAST(MAX(name = 'page_converted_date_moving') AS INT64) AS page_converted_date_moving
      , CAST(MAX(name = 'address') AS INT64) AS address
      , CAST(MAX(name = 'page_converted_address') AS INT64) AS page_converted_address
      , CAST(MAX(name = 'offers') AS INT64) AS offers
      , CAST(MAX(name = 'page_converted_offers') AS INT64) AS page_converted_offers
      , CAST(MAX(name = 'personal_info') AS INT64) AS personal_info
      , CAST(MAX(name = 'page_converted_personal_info') AS INT64) AS page_converted_personal_info
      , CAST(MAX(name = 'code_activation') AS INT64) AS code_activation
      , CAST(MAX(name = 'page_converted_code_activation') AS INT64) AS page_converted_code_activation
      , CAST(MAX(name = 'payment') AS INT64) AS payment
      , CAST(MAX(name = 'page_converted_payment') AS INT64) AS page_converted_payment
      , CAST(MAX(name = 'summary') AS INT64) AS summary
      , CAST(MAX(name = 'page_converted_summary') AS INT64) AS page_converted_summary

      FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`

      WHERE context_page_path LIKE '%mon-compte/courrier%'

      GROUP BY id_app_user
    ) USING(id_app_user)

    # device and browser 
    LEFT JOIN ( 
      SELECT 
        context_user_agent
      , ANY_VALUE(browser) AS browser
      , ANY_VALUE(browser_version) AS browser_version
      , ANY_VALUE(os) AS os
      , ANY_VALUE(os_version) AS os_version

      , CASE
          WHEN ANY_VALUE(pc) THEN 'desktop'
          WHEN ANY_VALUE(tablet) THEN 'tablet'
          WHEN ANY_VALUE(mobile) THEN 'mobile'
        END AS device
        
      , ANY_VALUE(device_brand) AS device_brand
      , ANY_VALUE(device_model) AS device_model

      FROM `souscritoo-1343.Views.product_ar_useragent` 

      GROUP BY context_user_agent
    ) USING(context_user_agent)

    # validation et client_e 
    LEFT JOIN (
      SELECT
        CAST(id_app_user AS STRING) AS id_app_user
      , MAX(IF(
          fact_table_app_contract.id_dim_created_date < 20180818
        , valid
        , valid AND origin_contract ='app'
        )) AS valid
      , MAX(
          CASE
            WHEN fact_table_app_contract.id_dim_created_date < 20180818 AND valid
              THEN conversion_probability
            WHEN valid AND origin_contract ='app'
              THEN conversion_probability
            ELSE NULL
          END
        ) AS client_e

      FROM `souscritoo-1343.star_schema.fact_table_app_contract` AS fact_table_app_contract
      
      LEFT JOIN `souscritoo-1343.star_schema.fact_table_contract`
        USING (id_dim_sf_opportunity)

      WHERE id_dim_app_contract_poste IS NOT NULL

      GROUP BY id_app_user
    ) USING(id_app_user)
        
    # Attrib
    LEFT JOIN (
      SELECT
        id_app_user  
      , source
      , source2  
      FROM `souscritoo-1343.star_schema.attribution` 
    ) USING(id_app_user)
  )
  
SELECT #somme des prospects par jour device on pourrait rajouter les bu style produit 
  date 
, id_text 
, device 
, source
, source2
, COUNT(id_app_user) AS app_user
, SUM(address) AS address
, SUM(address_conso) AS address_conso
, SUM(date_moving) AS date_moving
, SUM(date_moving_conso) AS date_moving_conso
, SUM(personal_info) AS personal_info
, SUM(personal_info_conso) AS personal_info_conso
, SUM(offers) AS offers
, SUM(offers_conso) AS offers_conso
, SUM(code_activation) AS code_activation
, SUM(payment) AS payment
, SUM(code_activation_conso) AS code_activation_conso
, SUM(payment_conso) AS payment_conso
, SUM(summary) AS summary
, SUM(summary_conso) AS summary_conso
, SUM(valid) AS valid
, SUM(client_e) AS client_e

FROM table 
GROUP BY date, id_text, device, source , source2