#souscritoo-1343:Views.product_ar_test_education
WITH table AS (
  SELECT 
    id_app_user 
  , date 
  , id_text 
  , source
  , source2
  , device

  # consolidation avec les pages converted
  , IF(who_we_are = 1,1,page_converted_who_we_are) AS who_we_are
  , IF(next_subscription = 1,1,page_converted_next_subscription) AS next_subscription
  , IF(time_gain = 1,1,page_converted_time_gain) AS time_gain

  , page_converted_who_we_are as who_we_are_conso
  , page_converted_next_subscription as next_subscription_conso
  , page_converted_time_gain as time_gain_conso
  
  FROM ( # dim_app_user 
    SELECT
      CAST(id_app_user AS STRING) AS id_app_user   
    , id_text 
    , DATE(date_joined) AS date 
    FROM `souscritoo-1343.star_schema.dimension_app_user`
    WHERE DATE(date_joined) >= '2018-07-18'
  )

  JOIN (
    SELECT 
      id_universal AS id_app_user 
    , ANY_VALUE(context_user_agent) AS context_user_agent
    , CAST(MAX(name LIKE 'who_we_are') AS INT64) AS who_we_are
    , CAST(MAX(name LIKE 'page_converted_who_we_are') AS INT64) AS page_converted_who_we_are
    , CAST(MAX(name LIKE 'next_subscription') AS INT64) AS next_subscription
    , CAST(MAX(name LIKE 'page_converted_next_subscription' ) AS INT64) AS page_converted_next_subscription
    , CAST(MAX(name LIKE 'time_gain') AS INT64) AS time_gain
    , CAST(MAX(
        name LIKE 'page_converted_time_gain' AND id_dim_date > 20181107 
        OR name LIKE 'dashboard_energy_valid_only' AND id_dim_date <= 20181107 
      ) AS INT64) AS page_converted_time_gain

    FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`
    GROUP BY id_app_user
    HAVING MAX(url LIKE '%mon-compte/qui-sommes-nous%') 
  ) USING(id_app_user)

  # Attrib
  LEFT JOIN (
    SELECT
      id_app_user  
    , source
    , source2

    FROM `souscritoo-1343.star_schema.attribution` 
  ) USING(id_app_user)

  # device and browser 
  LEFT JOIN ( 
    SELECT 
      context_user_agent
    , ANY_VALUE(browser) AS browser
    , ANY_VALUE(browser_version) AS browser_version
    , ANY_VALUE(os) AS os
    , ANY_VALUE(os_version) AS os_version

    , CASE
        WHEN ANY_VALUE(pc) THEN 'desktop'
        WHEN ANY_VALUE(tablet) THEN 'tablet'
        WHEN ANY_VALUE(mobile) THEN 'mobile'
      END AS device
      
    , ANY_VALUE(device_brand) AS device_brand
    , ANY_VALUE(device_model) AS device_model

    FROM `souscritoo-1343.Views.product_ar_useragent` 

    GROUP BY context_user_agent
  ) USING (context_user_agent)
)
  
SELECT #somme des prospects par jour device on pourrait rajouter les bu style produit 
  date 
, id_text 
, source
, source2
, device
, COUNT(id_app_user) AS app_user
, SUM(who_we_are_conso) AS who_we_are_conso
, SUM(who_we_are) AS who_we_are
, SUM(next_subscription) AS next_subscription
, SUM(next_subscription_conso) AS next_subscription_conso
, SUM(time_gain) AS time_gain
, SUM(time_gain_conso) AS time_gain_conso

FROM table 
GROUP BY date, id_text, source , source2 , device
