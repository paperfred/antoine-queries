#souscritoo-1343:Views.product_ab_test_box_datastudio
#Dashboard : Funnel App Only
WITH table AS (
  SELECT 
    user.id_app_user 
  , date
  , source
  , source2
  , id_text 
  , device 
  , client_e
  , energy_contract.id_app_user IS NOT NULL validated_energy_funnel
  , IF(is_moving=1,1,page_converted_is_moving ) AS is_moving 
  , page_converted_is_moving 
  , IF(address=1,1,page_converted_address) AS address
  , IF(sup_address=1,1,page_converted_sup_address ) AS sup_address 
  , IF(date_moving=1,1,page_converted_date_moving) AS date_moving
  , IF(personal_info=1,1,page_converted_personal_info) AS personal_info
  , IF(offers=1,1,page_converted_offers ) AS offers 
  , IF(payment=1,1,page_converted_payment) AS payment
  , IF(summary=1,1,page_converted_summary) AS summary
  
  , IF(date >= '2018-08-24',page_converted_address,IF((sup_address+date_moving+personal_info+offers+summary)>=1,address,0)) AS address_conso  
  , IF(date >= '2018-08-24',page_converted_sup_address,IF((date_moving+personal_info+offers+payment+summary)>=1,sup_address,0)) AS sup_address_conso
  , IF(date >= '2018-08-24',page_converted_date_moving,IF((personal_info+offers+payment+summary)>=1,date_moving,0)) AS date_moving_conso
  , IF(date >= '2018-08-24',page_converted_personal_info,IF((offers+payment+summary)>=1,personal_info,0)) AS personal_info_conso
  , IF(date >= '2018-08-24',page_converted_offers,IF((payment+summary)>=1,offers,0)) AS offers_conso
  , IF(date >= '2018-08-24',page_converted_payment,IF((summary)>=1,payment,0)) AS payment_conso
  , IF(date >= '2018-08-24',page_converted_summary,IF(valid = TRUE,summary,0)) AS summary_conso 
  , IF(date < '2018-08-19', IF(valid, summary, 0), CAST(IFNULL(valid, FALSE) AS INT64)) AS valid  # avant le 19 on prend summary_conso après on prend le valid + origin = app

  # dim_app_user
  FROM (
    SELECT
      CAST(id_app_user AS STRING) AS id_app_user   
    , id_text AS id_text
    , DATE(date_joined) AS date 
    , TIME(date_joined) AS time 

    FROM `souscritoo-1343.star_schema.dimension_app_user`
  ) AS user

  # pages
  JOIN (
    SELECT 
      id_universal AS id_app_user
    , ANY_VALUE(context_user_agent) AS context_user_agent
    , MIN(DATETIME(date_actual, time)) AS began_funnel_datetime
    , CAST(MAX(name LIKE 'address') AS INT64) AS address
    , CAST(MAX(name LIKE 'sup_address') AS INT64) AS sup_address
    , CAST(MAX(name LIKE 'is_moving') AS INT64) AS is_moving
    , CAST(MAX(name LIKE 'offers') AS INT64) AS offers
    , CAST(MAX(name LIKE 'date_moving') AS INT64) AS date_moving
    , CAST(MAX(name LIKE 'personal_info') AS INT64) AS personal_info
    , CAST(MAX(name LIKE 'payment') AS INT64) AS payment
    , CAST(MAX(name LIKE 'summary') AS INT64) AS summary

    , CAST(MAX(name LIKE 'page_converted_address') AS INT64) AS page_converted_address
    , CAST(MAX(name LIKE 'page_converted_sup_address') AS INT64) AS page_converted_sup_address
    , CAST(MAX(name LIKE 'page_converted_is_moving') AS INT64) AS page_converted_is_moving
    , CAST(MAX(name LIKE 'page_converted_offers') AS INT64) AS page_converted_offers
    , CAST(MAX(name LIKE 'page_converted_date_moving') AS INT64) AS page_converted_date_moving
    , CAST(MAX(name LIKE 'page_converted_personal_info') AS INT64) AS page_converted_personal_info
    , CAST(MAX(name LIKE 'page_converted_payment') AS INT64) AS page_converted_payment
    , CAST(MAX(name LIKE 'page_converted_summary') AS INT64) AS page_converted_summary

    FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`

    JOIN `souscritoo-1343.star_schema.dimension_date` 
      USING (id_dim_date)
    
    JOIN `souscritoo-1343.star_schema.dimension_time` 
      USING (id_dim_time)

    WHERE url LIKE '%mon-compte/box%'

    GROUP BY id_app_user
  ) USING(id_app_user)

  # device and browser 
  LEFT JOIN ( 
    SELECT 
      context_user_agent
    , ANY_VALUE(browser) AS browser
    , ANY_VALUE(browser_version) AS browser_version
    , ANY_VALUE(os) AS os
    , ANY_VALUE(os_version) AS os_version

    , CASE
        WHEN ANY_VALUE(pc) THEN 'desktop'
        WHEN ANY_VALUE(tablet) THEN 'tablet'
        WHEN ANY_VALUE(mobile) THEN 'mobile'
      END AS device
      
    , ANY_VALUE(device_brand) AS device_brand
    , ANY_VALUE(device_model) AS device_model

    FROM `souscritoo-1343.Views.product_ar_useragent` 

    GROUP BY context_user_agent
  ) USING(context_user_agent)

  # validation et client_e 
  LEFT JOIN (
    SELECT
      CAST(id_app_user AS STRING) AS id_app_user
    , MAX(IF(
        fact_table_app_contract.id_dim_created_date < 20180818
      , valid
      , valid AND origin_contract ='app'
      )) AS valid
    , MAX(
        CASE
          WHEN fact_table_app_contract.id_dim_created_date < 20180818 AND valid
            THEN conversion_probability
          WHEN valid AND origin_contract ='app'
            THEN conversion_probability
          ELSE NULL
        END
      ) AS client_e

    FROM `souscritoo-1343.star_schema.fact_table_app_contract` AS fact_table_app_contract
    
    LEFT JOIN `souscritoo-1343.star_schema.fact_table_contract`
      USING (id_dim_sf_opportunity)

    WHERE id_dim_app_contract_box IS NOT NULL

    GROUP BY id_app_user
  ) USING(id_app_user)
   
  # BU 
  LEFT JOIN (
    SELECT
      id_app_user  
    , source
    , source2
    
    FROM `souscritoo-1343.star_schema.attribution` 
  ) USING(id_app_user)

  # Previous energy funnel
  LEFT JOIN (
    SELECT
      CAST(id_app_user AS STRING) AS id_app_user
    , MIN(DATETIME(date_actual, time)) AS began_energy_datetime

    FROM `souscritoo-1343.star_schema.fact_table_app_contract` 

    JOIN `souscritoo-1343.star_schema.dimension_date` 
      ON id_dim_date = id_dim_created_date

    JOIN `souscritoo-1343.star_schema.dimension_time` 
      ON id_dim_time = id_dim_created_time
          
    WHERE id_dim_app_contract_energy IS NOT NULL
      AND (
        valid AND origin_contract = 'app' AND id_dim_created_date >= 20180818
        OR valid AND id_dim_created_date >= 20180818
      )
      
    GROUP BY id_app_user
  ) AS energy_contract 
    ON user.id_app_user = energy_contract.id_app_user
    AND began_energy_datetime < began_funnel_datetime
)
  
SELECT 
  date 
, id_text 
, device 
, source
, source2
, validated_energy_funnel
, COUNT(id_app_user) AS app_user
, SUM(is_moving) AS is_moving 
, SUM(page_converted_is_moving) AS page_converted_is_moving
, SUM(address) AS address
, SUM(address_conso) AS address_conso
, SUM(sup_address) AS sup_address
, SUM(sup_address_conso) AS sup_address_conso
, SUM(date_moving) AS date_moving
, SUM(date_moving_conso) AS date_moving_conso
, SUM(personal_info) AS personal_info
, SUM(personal_info_conso) AS personal_info_conso
, SUM(offers) AS offers
, SUM(offers_conso) AS offers_conso
, SUM(payment) AS payment
, SUM(payment_conso) AS payment_conso
, SUM(summary) AS summary
, SUM(summary_conso) AS summary_conso
, SUM(valid) AS valid
, SUM(client_e) as client_e
FROM table 

WHERE date >= '2018-07-18'

GROUP BY date, id_text, device, source , source2, validated_energy_funnel
