#souscritoo-1343:Views.product_ab_test_data_studio
WITH 
  table AS (
    SELECT 
      id_app_user 
    , date
    , is_presented
    , id_text 
    , device 
    , business_unit
    , source
    , source2
    , client_e    
    , user_is_moving
    , user_energy_type
    , user_pro_residence
    , user_electricity_cut
    , user_gas_cut
    , user_energy_status
    , user_has_pce
    , user_has_pdl
    , user_payment_method
    , user_first_tenant
    , subscribed_provider
    , provider AS chosen_provider
    # page consolide 
    , IF(energy_type = 1 , 1 , page_converted_energy_type) AS energy_type
    , IF(equipment = 1 , 1 , page_converted_equipment) AS equipment
    , IF(is_moving = 1 , 1 , page_converted_is_moving) AS is_moving 
    , IF(house = 1 , 1 , page_converted_house) AS house
    , IF(address = 1 , 1 , page_converted_address) AS address
    , IF(address_detailed = 1 , 1 , page_converted_address_detailed) AS address_detailed 
    , IF(date_moving = 1 , 1 , page_converted_date_moving) AS date_moving
    , IF(personal_info = 1 , 1 , page_converted_personal_info) AS personal_info
    , IF(offers = 1 , 1 , page_converted_offers) AS offers 
    , IF(payment = 1 , 1 , page_converted_payment) AS payment
    , IF(elec_settings = 1 , 1 , page_converted_elec_settings) AS elec_settings 
    , IF(gas_settings = 1 , 1 , page_converted_gas_settings) AS gas_settings
    , IF(sup_address = 1 , 1 , page_converted_sup_address) AS sup_address
    , IF(summary = 1 , 1 , page_converted_summary) AS summary
    # consolidation 
    , IF(date > '2018-08-24',page_converted_energy_type,IF((equipment+is_moving+house+address+address_detailed+date_moving+personal_info+offers+payment+elec_settings+gas_settings+sup_address+summary)>=1,energy_type,0)) AS energy_type_conso 
    , IF(date > '2018-08-24',page_converted_equipment,IF((is_moving+house+address+address_detailed+date_moving+personal_info+offers+payment+elec_settings+gas_settings+sup_address+summary)>=1,equipment,0)) AS equipment_conso
    , IF(date > '2018-08-24',page_converted_is_moving,IF((house+address+address_detailed+date_moving+personal_info+offers+payment+elec_settings+gas_settings+sup_address+summary)>=1,is_moving,0)) AS is_moving_conso
    , IF(date > '2018-08-24',page_converted_house  ,IF((address+address_detailed+date_moving+personal_info+offers+payment+elec_settings+gas_settings+sup_address+summary)>=1,house,0)) AS house_conso
    , IF(date > '2018-08-24',page_converted_address,IF((address_detailed+date_moving+personal_info+offers+payment+elec_settings+gas_settings+sup_address+summary)>=1,address,0)) AS address_conso
    , IF(date > '2018-08-24',page_converted_address_detailed,IF((date_moving+personal_info+offers+payment+elec_settings+gas_settings+sup_address+summary)>=1,address_detailed,0)) AS address_detailed_conso
    , IF(date > '2018-08-24',page_converted_date_moving,IF((personal_info+offers+payment+elec_settings+gas_settings+sup_address+summary)>=1,date_moving,0)) AS date_moving_conso
    , IF(date > '2018-08-24',page_converted_personal_info,IF((offers+payment+elec_settings+gas_settings+sup_address+summary)>=1,personal_info,0)) AS personal_info_conso
    , IF(date > '2018-08-24',page_converted_offers,IF((payment+elec_settings+gas_settings+sup_address+summary)>=1,offers,0)) AS offers_conso
    , IF(date > '2018-08-24',page_converted_payment,IF((elec_settings+gas_settings+sup_address+summary)>=1,payment,0)) AS paymentconso
    , IF(date > '2018-08-24',page_converted_elec_settings,IF((gas_settings+sup_address+summary)>=1,elec_settings,0)) AS elec_settings_conso 
    , IF(date > '2018-08-24',page_converted_gas_settings,IF((sup_address+summary)>=1,gas_settings,0)) AS gas_settings_conso
    , IF(date > '2018-08-24',page_converted_sup_address,IF((summary)>=1,sup_address,0)) AS sup_address_conso
    , IF(date > '2018-08-24',page_converted_summary,IF(valid = TRUE,summary,0)) AS summary_conso  
    , IF(date < '2018-08-19',IF(valid = TRUE,summary,0),CAST(IFNULL(valid,FALSE) AS INT64)) AS valid  # avant le 19 on prend summary_conso après on prend le valid + origin = app
  # pas besoin de consolidation pour les pages a venir on utilise juste page_converted_...

    FROM (
     # dim_app_user
      SELECT
        CAST(id_app_user AS STRING) AS id_app_user
      , user_email IS NOT NULL
          AND user_phone IS NOT NULL 
          AND user_first_name IS NOT NULL
          AND user_last_name IS NOT NULL
        AS filled_name
      , id_address IS NOT NULL AS filled_address
      , date_leaving IS NOT NULL AS date_leaving
      , id_text
      , DATE(date_joined) AS date 
      , TIME( date_joined ) AS time 
      , is_moving AS user_is_moving
      , pro_residence AS user_pro_residence
      , energy_status AS user_energy_status
      , first_tenant AS user_first_tenant

      FROM `souscritoo-1343.star_schema.dimension_app_user`
      WHERE DATE(date_joined) > '2018-06-21'
    )

    JOIN (
    #cast 
      SELECT 
        id_app_user
      , CAST(is_moving AS INT64) AS is_moving
      , CAST(energy_type AS INT64) AS energy_type
      , CAST(equipment AS INT64) AS equipment
      , CAST(house AS INT64) AS house  
      , CAST(address AS INT64) AS address
      , CAST(address_detailed AS INT64) AS address_detailed
      , CAST(date_moving AS INT64) AS date_moving
      , CAST(personal_info AS INT64) AS personal_info
      , CAST(offers AS INT64) AS offers
      , CAST(payment AS INT64) AS payment
      , CAST(sup_address AS INT64) AS sup_address
      , CAST(elec_settings AS INT64) AS elec_settings
      , CAST(gas_settings AS INT64) AS gas_settings
      , CAST(summary AS INT64) AS summary
      , CAST(page_converted_is_moving AS INT64) AS page_converted_is_moving
      , CAST(page_converted_energy_type AS INT64) AS page_converted_energy_type
      , CAST(page_converted_equipment AS INT64) AS page_converted_equipment
      , CAST(page_converted_house AS INT64) AS page_converted_house  
      , CAST(page_converted_address AS INT64) AS page_converted_address
      , CAST(page_converted_address_detailed AS INT64) AS page_converted_address_detailed
      , CAST(page_converted_date_moving AS INT64) AS page_converted_date_moving
      , CAST(page_converted_personal_info AS INT64) AS page_converted_personal_info
      , CAST(page_converted_offers AS INT64) AS page_converted_offers
      , CAST(page_converted_payment AS INT64) AS page_converted_payment
      , CAST(page_converted_sup_address AS INT64) AS page_converted_sup_address
      , CAST(page_converted_elec_settings AS INT64) AS page_converted_elec_settings
      , CAST(page_converted_gas_settings AS INT64) AS page_converted_gas_settings
      , CAST(page_converted_summary AS INT64) AS page_converted_summary
      FROM `souscritoo-1343.Views.product_app_ab_pages` #pages energie si on rajoute une nouvelle page il faut le faire aussi ici 
    ) USING(id_app_user)

    # validation  
    LEFT JOIN (
      SELECT
        CAST(id_app_user AS STRING) AS id_app_user
      , MAX(IF(
          fact_table_app_contract.id_dim_created_date < 20180818
        , fact_table_app_contract.valid
        , fact_table_app_contract.valid AND fact_table_app_contract.origin_contract ='app'
        )) AS valid
      , MAX(
          CASE
            WHEN fact_table_app_contract.id_dim_created_date < 20180818 AND fact_table_app_contract.valid
              THEN conversion_probability
            WHEN fact_table_app_contract.valid AND fact_table_app_contract.origin_contract ='app'
              THEN conversion_probability
            ELSE NULL
          END
        ) AS client_e
      , ANY_VALUE(energy_type) AS user_energy_type
      , ANY_VALUE(electricity_cut) AS user_electricity_cut
      , ANY_VALUE(gas_cut) AS user_gas_cut
      , ANY_VALUE(has_pce) AS user_has_pce
      , ANY_VALUE(has_pdl) AS user_has_pdl
      , ANY_VALUE(user_payment_method) AS user_payment_method
      , ANY_VALUE(provider) AS subscribed_provider

      FROM `souscritoo-1343.star_schema.fact_table_app_contract` AS fact_table_app_contract
      
      LEFT JOIN `souscritoo-1343.star_schema.fact_table_contract`
        USING (id_dim_sf_opportunity)

      JOIN `souscritoo-1343.star_schema.dimension_app_contract_energy`
        USING (id_dim_app_contract_energy)

      GROUP BY id_app_user
    ) USING(id_app_user)
     # BU 

     LEFT JOIN (
      #attribution bu facon produit 
        SELECT
          id_app_user
        , CAST(is_presented AS INT64) AS is_presented  
        , business_unit
        , source
        , source2
        , device
        , browser
        
        FROM `souscritoo-1343.star_schema.attribution` 
      ) USING(id_app_user)

    LEFT JOIN (
      SELECT 
        id_universal AS id_app_user
      , provider
      FROM (
        SELECT 
          id_universal
        , provider
        , ROW_NUMBER() OVER (PARTITION BY id_universal ORDER BY id_dim_date, id_dim_time DESC) AS rn
        FROM `souscritoo-1343.star_schema.fact_table_tracking_clicked_offer` 
        WHERE context_page_path LIKE '%mon-compte/energie%'
      ) WHERE rn = 1 AND id_universal IS NOT NULL
    ) AS click_offers
      USING (id_app_user)
  )
  
SELECT # somme par jour device id_text, bu 
  date 
, id_text 
, device 
, business_unit
, source
, source2
, user_is_moving
, user_energy_type
, user_pro_residence
, user_electricity_cut
, user_gas_cut
, user_energy_status
, user_has_pce
, user_has_pdl
, user_payment_method
, user_first_tenant
, subscribed_provider
, chosen_provider
, COUNT(id_app_user) AS app_user
, SUM(is_presented) AS presented_prospect
, SUM(valid) AS valid
, SUM(client_e) AS client_e
, SUM(energy_type) AS energy_type 
, SUM(energy_type_conso) AS energy_type_conso
, SUM(equipment) AS equipment
, SUM(equipment_conso) AS equipment_conso
, SUM(is_moving) AS is_moving
, SUM(is_moving_conso) AS is_moving_conso
, SUM(house) AS house
, SUM(house_conso) AS house_conso
, SUM(address) AS address
, SUM(address_conso) AS address_conso
, SUM(address_detailed) AS address_detailed
, SUM(address_detailed_conso) AS address_detailed_conso
, SUM(date_moving) AS date_moving
, SUM(date_moving_conso) AS date_moving_conso
, SUM(personal_info) AS personal_info
, SUM(personal_info_conso) AS personal_info_conso
, SUM(offers) AS offers
, SUM(offers_conso) AS offers_conso
, SUM(payment) AS payment
, SUM(paymentconso) AS paymentconso
, SUM(elec_settings) AS elec_settings
, SUM(elec_settings_conso) AS elec_settings_conso
, SUM(gas_settings) AS gas_settings
, SUM(gas_settings_conso) AS gas_settings_conso
, SUM(sup_address) AS sup_address
, SUM(sup_address_conso) AS sup_address_conso
, SUM(summary) AS summary
, SUM(summary_conso) AS summary_conso

FROM table 

GROUP BY 
  date, id_text, device, business_unit, source, source2, 
  user_is_moving, user_energy_type, user_pro_residence, 
  user_electricity_cut, user_gas_cut, user_energy_status, 
  user_has_pce, user_has_pdl, user_payment_method, subscribed_provider,
  chosen_provider, user_first_tenant