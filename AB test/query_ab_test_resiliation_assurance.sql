#souscritoo-1343:Views.product_ab_test_insurance_cancellation
WITH table AS 
  (
    SELECT 
      id_app_user 
    , date 
    , id_text 
    , device 
    ,	IF(insurance_situation=1,1,page_converted_insurance_situation) AS insurance_situation
    ,	IF(about_you=1,1,page_converted_about_you) AS about_you
    ,	IF(contract_references=1,1,page_converted_contract_references) AS contract_references
    ,	IF(summary=1,1,page_converted_summary) AS summary
    ,	page_converted_insurance_situation
    ,	page_converted_about_you
    ,	page_converted_contract_references
    ,	page_converted_summary
    , resiliation_assurance_valid 

    FROM # dim_app_user
      (
      SELECT
        CAST(id_app_user AS STRING) AS id_app_user   
      , MAX(
               ( user_email IS NOT NULL 
                AND user_phone IS NOT NULL 
                AND user_first_name IS NOT NULL
                AND user_last_name IS NOT NULL)
              ) AS filled_name 
      , MAX(id_address IS NOT NULL) AS filled_address
      , MAX(date_leaving IS NOT NULL) AS date_leaving 
      , id_text 
      , DATE(date_joined) AS date 
      , TIME( date_joined ) AS time 
      , IFNULL(is_moving,FALSE) AS is_moving_bool

      FROM `souscritoo-1343.star_schema.dimension_app_user`
      GROUP BY id_app_user, id_text, date, time, is_moving
      )

    JOIN
      (
      SELECT 
        id_app_user 
      ,	CAST (insurance_situation AS INT64) AS insurance_situation
      ,	CAST (about_you AS INT64) AS about_you
      ,	CAST (contract_references AS INT64) AS contract_references
      ,	CAST (summary AS INT64) AS summary
      ,	CAST (page_converted_insurance_situation AS INT64) AS page_converted_insurance_situation
      ,	CAST (page_converted_about_you AS INT64) AS page_converted_about_you
      ,	CAST (page_converted_contract_references AS INT64) AS page_converted_contract_references
      ,	CAST (page_converted_summary AS INT64) AS page_converted_summary
      FROM `souscritoo-1343.Views.product_app_ab_pages_insurance_cancellation` 
      )
      USING(id_app_user)

    LEFT JOIN # device and browser 
      ( 
      SELECT 
          id_app_user
        , CASE
              WHEN REGEXP_CONTAINS(context_user_agent, r'\(iPad')
              OR REGEXP_CONTAINS(context_user_agent, r'(SM-T.*)Build/[^)]+\)') -- System: Galaxy Note
              OR REGEXP_CONTAINS(context_user_agent, r'Tablet') 
                THEN 'tablet'
              WHEN REGEXP_CONTAINS(context_user_agent, r'Windows NT') -- OS: Window
              OR REGEXP_CONTAINS(context_user_agent, r'Macintosh') -- OS: Mac OSX
              OR REGEXP_CONTAINS(context_user_agent, r'X11') -- OS: Linux
                THEN 'desktop'
              WHEN REGEXP_CONTAINS(LOWER(context_user_agent), r'playstation|nintendo')
                THEN 'other'
              ELSE 'mobile'
            END AS device
            /* B R O W S E R */
        , CASE
              WHEN REGEXP_CONTAINS(context_user_agent, 'Firefox/[[:digit:]]+[.]?[[:digit:]]*')
                AND NOT REGEXP_CONTAINS(context_user_agent, 'Seamonkey/[[:digit:]]+[.]?[[:digit:]]*')
                THEN 'Firefox'
              WHEN REGEXP_CONTAINS(context_user_agent, 'Seamonkey/[[:digit:]]+[.]?[[:digit:]]*')
                THEN 'Seamonkey'
              WHEN REGEXP_CONTAINS(context_user_agent, 'Chrome/[[:digit:]]+[.]?[[:digit:]]*')
                AND NOT REGEXP_CONTAINS(context_user_agent, 'Chromium/[[:digit:]]+[.]?[[:digit:]]*')
                THEN 'Chrome'
              WHEN REGEXP_CONTAINS(context_user_agent, 'Chromium/[[:digit:]]+[.]?[[:digit:]]*')
                THEN 'Chromium'
              WHEN REGEXP_CONTAINS(context_user_agent, '[^(?:Mobile)].* Safari/[[:digit:]]+[.]?[[:digit:]]*')
                AND NOT REGEXP_CONTAINS(context_user_agent, 'Chrom(?:e)|(?:ium)/[[:digit:]]+[.]?[[:digit:]]*')
                THEN 'Safari'
              WHEN REGEXP_CONTAINS(context_user_agent, '(?:OPR)|(?:Opera)/[[:digit:]]+[.]?[[:digit:]]*')
                THEN 'Opera'
              WHEN REGEXP_CONTAINS(context_user_agent, 'FBAV/[[:digit:]]+[.]?[[:digit:]]*')
                THEN 'Facebook Browser'
              WHEN REGEXP_CONTAINS(context_user_agent, 'AppleWebKit/[[:digit:]]+[.]?[[:digit:]]*')
                THEN 'Webkit-based browser'
              WHEN REGEXP_CONTAINS(context_user_agent, '; MSIE [[:digit:]]+[.]?[[:digit:]]*;') 
                OR REGEXP_CONTAINS(context_user_agent, '[.]?NET')
                OR REGEXP_CONTAINS(context_user_agent, 'IEMobile/[[:digit:]]+[.]?[[:digit:]]*')
                OR REGEXP_CONTAINS(context_user_agent, '; rv:[[:digit:]]+[.]?[[:digit:]]*')
                THEN 'Internet Explorer'
              END AS browser
        FROM     
           (
           SELECT      
              DISTINCT user_id AS id_app_user 
            , FIRST_VALUE(context_user_agent IGNORE NULLS) OVER (PARTITION BY user_id ORDER BY timestamp ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ) AS context_user_agent 
           FROM `souscritoo-1343.prod_app_sct.identifies` 
            )
      )
      USING(id_app_user)
      LEFT JOIN 
      (
      SELECT 
        id_prospect 
      , id_app_user 
      FROM `souscritoo-1343.star_schema.attribution` 
      )
      USING(id_app_user)
     LEFT JOIN #cancellation 
     (
      SELECT 
        id_prospect   
      , MAX(IF(REGEXP_CONTAINS(recordtype_name,'MRH') AND status = 'CANCEL_VALID',1,0)) AS resiliation_assurance_valid
      FROM `souscritoo-1343.star_schema.fact_table_cancellations` 
      WHERE cancelled_from_app #true = app
      GROUP BY 1
      ORDER BY id_prospect 
     )
     USING(id_prospect)
    ORDER BY date DESC
  )
  
SELECT 
  date 
, id_text 
, device 
, COUNT(id_app_user) AS app_user
,	SUM(insurance_situation) AS insurance_situation
,	SUM(about_you) AS about_you
,	SUM(contract_references) AS contract_references
,	SUM(summary) AS summary
,	SUM(page_converted_insurance_situation) AS page_converted_insurance_situation
,	SUM(page_converted_about_you) AS page_converted_about_you
,	SUM(page_converted_contract_references) AS page_converted_contract_references
,	SUM(page_converted_summary) AS page_converted_summary	
FROM table 
WHERE date >= '2018-08-31'
GROUP BY date, id_text, device  
ORDER BY date DESC