SELECT
  CAST(id_universal AS STRING) AS id_app_user
, MAX(name = 'is_moving') AS is_moving
, MAX(name = 'page_converted_is_moving') AS page_converted_is_moving
, MAX(name = 'house') AS house
, MAX(name = 'page_converted_house') AS page_converted_house
, MAX(name = 'inhabitants') AS inhabitants
, MAX(name = 'page_converted_inhabitants') AS page_converted_inhabitants
, MAX(name = 'assets_value') AS assets_value
, MAX(name = 'page_converted_assets_value') AS page_converted_assets_value
, MAX(name = 'particularities') AS particularities
, MAX(name = 'page_converted_particularities') AS page_converted_particularities
, MAX(name = 'contract_begin') AS contract_begin
, MAX(name = 'page_converted_contract_begin') AS page_converted_contract_begin
, MAX(name = 'address') AS address
, MAX(name = 'page_converted_address') AS page_converted_address
, MAX(name = 'personal_info') AS personal_info
, MAX(name = 'page_converted_personal_info') AS page_converted_personal_info
, MAX(name = 'offers') AS offers
, MAX(name = 'page_converted_offers') AS page_converted_offers
, MAX(name = 'payment') AS payment
, MAX(name = 'page_converted_payment') AS page_converted_payment
, MAX(name = 'summary') AS summary
, MAX(name = 'page_converted_summary') AS page_converted_summary

FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 

WHERE url LIKE '%/mon-compte/assurance%'
GROUP BY id_app_user