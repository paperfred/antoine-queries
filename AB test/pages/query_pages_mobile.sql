# souscritoo-1343:Views.product_app_ab_pages_mobile
WITH pages AS 
  (
  SELECT DISTINCT 
    id_universal AS id_app_user 
  , name
  , url 
  FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`
  LEFT JOIN `souscritoo-1343.star_schema.construction_uid` 
    ON id_universal = alias
  WHERE name IS NOT NULL AND id_dim_date > 20180716 AND REGEXP_CONTAINS(context_page_path,r'mobile')
  ORDER BY id_app_user
  )


SELECT 
  id_app_user
, MAX(IF(name LIKE 'offers',TRUE,FALSE)) AS offers
, MAX(IF(name LIKE 'page_converted_offers',TRUE,FALSE)) AS page_converted_offers
, MAX(IF(name LIKE 'personal_info',TRUE,FALSE)) AS personal_info
, MAX(IF(name LIKE 'page_converted_personal_info',TRUE,FALSE)) AS page_converted_personal_info
, MAX(IF(name LIKE 'address_cellular',TRUE,FALSE)) AS address_cellular
, MAX(IF(name LIKE 'page_converted_address_cellular',TRUE,FALSE)) AS page_converted_address_cellular
, MAX(IF(name LIKE 'number_cellular' ,TRUE,FALSE)) AS number_cellular 
, MAX(IF(name LIKE 'page_converted_number_cellular' ,TRUE,FALSE)) AS page_converted_number_cellular
, MAX(IF(name LIKE 'payment',TRUE,FALSE)) AS payment
, MAX(IF(name LIKE 'page_converted_payment',TRUE,FALSE)) AS page_converted_payment
, MAX(IF(name LIKE 'payment_sim',TRUE,FALSE)) AS payment_sim
, MAX(IF(name LIKE 'page_converted_payment_sim',TRUE,FALSE)) AS page_converted_payment_sim
, MAX(IF(name LIKE 'summary',TRUE,FALSE)) AS summary
, MAX(IF(name LIKE 'page_converted_summary',TRUE,FALSE)) AS page_converted_summary
/*
, MAX(IF(name LIKE 'address',TRUE,FALSE)) AS address
, MAX(IF(name LIKE 'address_detailed',TRUE,FALSE)) AS address_detailed
*/
FROM pages 
GROUP BY id_app_user