#souscritoo-1343:Views.product_app_ab_pages_insurance_cancellation
WITH pages AS 
  (
  SELECT DISTINCT 
    id_universal AS id_app_user 
  , name
  , url 
  FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`
  LEFT JOIN `souscritoo-1343.star_schema.construction_uid` 
    ON id_universal = alias
  WHERE name IS NOT NULL AND id_dim_date >= 20180717 AND REGEXP_CONTAINS(context_page_path,r'insurance_cancellation')
  ORDER BY id_app_user
  )


SELECT 
  id_app_user
, MAX(IF(name LIKE 'insurance_situation',TRUE,FALSE)) AS insurance_situation
, MAX(IF(name LIKE 'about_you',TRUE,FALSE)) AS about_you
, MAX(IF(name LIKE 'contract_references',TRUE,FALSE)) AS contract_references
, MAX(IF(name LIKE 'summary',TRUE,FALSE)) AS summary
, MAX(IF(name LIKE 'page_converted_insurance_situation',TRUE,FALSE)) AS page_converted_insurance_situation
, MAX(IF(name LIKE 'page_converted_about_you',TRUE,FALSE)) AS page_converted_about_you
, MAX(IF(name LIKE 'page_converted_contract_references',TRUE,FALSE)) AS page_converted_contract_references
, MAX(IF(name LIKE 'page_converted_summary',TRUE,FALSE)) AS page_converted_summary
FROM pages 
GROUP BY id_app_user