#souscritoo-1343:Views.product_app_ab_location_pages
SELECT DISTINCT 
  IFNULL(id_universal , id_anonymous) AS id_user  
, REGEXP_EXTRACT(context_page_path ,r'mon-compte/([a-zA-Z0-9_.+-]+)/') AS location 
, name 
, TIMESTAMP(DATETIME(date_actual,time)) AS timestamp 
FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 
JOIN `souscritoo-1343.star_schema.dimension_date` 
  USING(id_dim_date)
JOIN `souscritoo-1343.star_schema.dimension_time` 
  USING(id_dim_time)
WHERE id_dim_date >=20180623 AND  REGEXP_EXTRACT(context_page_path ,r'mon-compte/([a-zA-Z0-9_.+-]+)/') NOT IN ('profil','undefined') 
ORDER BY id_user, timestamp