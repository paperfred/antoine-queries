#souscritoo-1343:Views.product_app_ab_pages_box
SELECT 
  id_universal AS id_app_user
, MAX(name LIKE 'address') AS address
, MAX(name LIKE 'sup_address') AS sup_address
, MAX(name LIKE 'is_moving') AS is_moving
, MAX(name LIKE 'address_detailed') AS address_detailed
, MAX(name LIKE 'offers') AS offers
, MAX(name LIKE 'date_moving') AS date_moving
, MAX(name LIKE 'personal_info') AS personal_info
, MAX(name LIKE 'payment') AS payment
, MAX(name LIKE 'summary') AS summary

, MAX(name LIKE 'page_converted_is_moving') AS page_converted_is_moving
, MAX(name LIKE 'page_converted_address') AS page_converted_address
, MAX(name LIKE 'page_converted_sup_address') AS page_converted_sup_address
, MAX(name LIKE 'page_converted_address_detailed') AS page_converted_address_detailed
, MAX(name LIKE 'page_converted_offers') AS page_converted_offers
, MAX(name LIKE 'page_converted_date_moving') AS page_converted_date_moving
, MAX(name LIKE 'page_converted_personal_info') AS page_converted_personal_info
, MAX(name LIKE 'page_converted_payment') AS page_converted_payment
, MAX(name LIKE 'page_converted_summary') AS page_converted_summary

FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`

WHERE url LIKE '%mon-compte/box%'

GROUP BY id_app_user