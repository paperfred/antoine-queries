#souscritoo-1343:Views.product_app_ab_pagess
WITH 
  pages AS (
    SELECT DISTINCT 
      id_universal AS id_app_user 
    , name
    , url 
    FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`
    LEFT JOIN `souscritoo-1343.star_schema.construction_uid` 
      ON id_universal = alias
    WHERE name IS NOT NULL AND id_dim_date > 20180621 AND REGEXP_CONTAINS(context_page_path,r'(/onboarding|/mon-compte/energie/\d{0,2})')
    ORDER BY id_app_user
  )

SELECT 
  id_app_user
, MAX(name LIKE 'is_moving') AS is_moving
, MAX(name LIKE 'page_converted_is_moving') AS page_converted_is_moving
, MAX(name LIKE 'energy_type') AS energy_type
, MAX(name LIKE 'page_converted_energy_type') AS page_converted_energy_type
, MAX(name LIKE 'equipment') AS equipment
, MAX(name LIKE 'page_converted_equipment') AS page_converted_equipment
, MAX(name LIKE 'house') AS house
, MAX(name LIKE 'page_converted_house') AS page_converted_house
, MAX(name LIKE 'address') AS address
, MAX(name LIKE 'page_converted_address') AS page_converted_address
, MAX(name LIKE 'address_detailed') AS address_detailed
, MAX(name LIKE 'page_converted_address_detailed') AS page_converted_address_detailed
, MAX(name LIKE 'date_moving') AS date_moving
, MAX(name LIKE 'page_converted_date_moving') AS page_converted_date_moving
, MAX(name LIKE 'personal_info') AS personal_info
, MAX(name LIKE 'page_converted_personal_info') AS page_converted_personal_info
, MAX(name LIKE 'offers') AS offers
, MAX(name LIKE 'page_converted_offers') AS page_converted_offers
, MAX(name LIKE 'payment') AS payment
, MAX(name LIKE 'page_converted_payment') AS page_converted_payment
, MAX(name LIKE 'sup_address') AS sup_address
, MAX(name LIKE 'page_converted_sup_address') AS page_converted_sup_address
, MAX(name LIKE 'elec_settings') AS elec_settings
, MAX(name LIKE 'page_converted_elec_settings') AS page_converted_elec_settings
, MAX(name LIKE 'gas_settings') AS gas_settings
, MAX(name LIKE 'page_converted_gas_settings') AS page_converted_gas_settings
, MAX(name LIKE 'summary') AS summary
, MAX(name LIKE 'page_converted_summary') AS page_converted_summary

FROM pages 
GROUP BY id_app_user