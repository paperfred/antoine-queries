#souscritoo-1343:Views.product_app_ar_pages_education
SELECT 
  id_universal AS id_app_user 
, MAX(name LIKE 'who_we_are') AS who_we_are
, MAX(name LIKE 'next_subscription') AS next_subscription
, MAX(name = 'time_gain') AS time_gain
, MAX(name LIKE 'page_converted_who_we_are') AS page_converted_who_we_are
, MAX(name LIKE 'page_converted_next_subscription' ) AS page_converted_next_subscription
, MAX(name LIKE 'dashboard_energy_valid_only') AS dashboard_energy_valid_only

FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`
GROUP BY id_app_user
HAVING MAX(url LIKE '%mon-compte/qui-sommes-nous%')