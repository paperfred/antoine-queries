#souscritoo-1343:Views.product_app_ar_pages_onboarding
SELECT 
  id_universal AS id_app_user 
, MAX(IF(name LIKE 'first_name' ,TRUE,FALSE)) AS first_name
, MAX(IF(name LIKE 'address',TRUE,FALSE)) AS address
, MAX(IF(name LIKE 'date_moving',TRUE,FALSE)) AS date_moving
, MAX(IF(name LIKE 'email',TRUE,FALSE)) AS email
, MAX(name = 'summary') AS summary

, MAX(IF(name LIKE 'page_converted_first_name',TRUE,FALSE)) AS page_converted_first_name
, MAX(IF(name LIKE 'page_converted_address' ,TRUE,FALSE)) AS page_converted_address
, MAX(IF(name LIKE 'page_converted_date_moving',TRUE,FALSE)) AS page_converted_date_moving
, MAX(IF(name LIKE 'page_converted_email',TRUE,FALSE)) AS page_converted_email
, MAX(name = 'dashboard_zero_valid') AS dashboard_zero_valid

FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`
GROUP BY id_app_user
HAVING MAX(url LIKE '%mon-compte/intro%')