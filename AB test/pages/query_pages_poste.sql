#souscritoo-1343:Views.product_app_ab_pages_poste
WITH pages AS 
  (
  SELECT DISTINCT 
    id_universal AS id_app_user 
  , name
  , url 
  FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`
  LEFT JOIN `souscritoo-1343.star_schema.construction_uid` 
    ON id_universal = alias
  WHERE name IS NOT NULL AND id_dim_date > 20180716 AND REGEXP_CONTAINS(context_page_path,r'courrier')
  ORDER BY id_app_user
  )


SELECT 
  id_app_user
, MAX(IF(name LIKE 'address' OR name LIKE 'old_address' ,TRUE,FALSE)) AS address
, MAX(IF(name LIKE 'address_detailed' ,TRUE,FALSE)) AS address_detailed
, MAX(IF(name LIKE 'date_moving',TRUE,FALSE)) AS date_moving
, MAX(IF(name LIKE 'personal_info',TRUE,FALSE)) AS personal_info
, MAX(IF(name LIKE 'offers',TRUE,FALSE)) AS offers
, MAX(IF(name LIKE 'summary',TRUE,FALSE)) AS summary
, MAX(IF(name LIKE 'poste_situation',TRUE,FALSE)) AS poste_situation
, MAX(IF(name LIKE 'declaration',TRUE,FALSE)) AS declaration
, MAX(IF(name LIKE 'creation_compte_poste',TRUE,FALSE)) AS creation_compte_poste


, MAX(IF(name LIKE 'page_converted_address' OR name LIKE 'page_converted_old_address' ,TRUE,FALSE)) AS page_converted_address
, MAX(IF(name LIKE 'page_converted_address_detailed' ,TRUE,FALSE)) AS page_converted_address_detailed
, MAX(IF(name LIKE 'page_converted_date_moving',TRUE,FALSE)) AS page_converted_date_moving
, MAX(IF(name LIKE 'page_converted_personal_info',TRUE,FALSE)) AS page_converted_personal_info
, MAX(IF(name LIKE 'page_converted_offers',TRUE,FALSE)) AS page_converted_offers
, MAX(IF(name LIKE 'page_converted_summary',TRUE,FALSE)) AS page_converted_summary
, MAX(IF(name LIKE 'page_converted_poste_situation',TRUE,FALSE)) AS page_converted_poste_situation
, MAX(IF(name LIKE 'page_converted_declaration',TRUE,FALSE)) AS page_converted_declaration
, MAX(IF(name LIKE 'page_converted_creation_compte_poste',TRUE,FALSE)) AS page_converted_creation_compte_poste

FROM pages 
GROUP BY id_app_user