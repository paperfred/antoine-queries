#souscritoo-1343:Views.product_ar_test_onboarding
WITH 
  table AS (
    SELECT 
      id_app_user 
    , date 
    , id_text 
    , source
    , source2
    , device
    # consolidation avec les pages converted
    , IF(first_name = 1,1,page_converted_first_name) AS first_name
    , IF(address = 1,1,page_converted_address) AS address
    , IF(date_moving = 1,1,page_converted_date_moving) AS date_moving
    , IF(email = 1,1,page_converted_email) AS email
    , IF(summary = 1,1,dashboard_zero_valid) AS summary

    , page_converted_first_name as first_name_conso
    , page_converted_address as address_conso
    , page_converted_date_moving as date_moving_conso
    , page_converted_email as email_conso
    , dashboard_zero_valid as summary_conso
  
  FROM (# dim_app_user 
    SELECT
      CAST(id_app_user AS STRING) AS id_app_user   
    , id_text 
    , DATE(date_joined) AS date 
    FROM `souscritoo-1343.star_schema.dimension_app_user`
    WHERE DATE(date_joined) >= '2018-09-30'
  )

  JOIN (
    SELECT 
      id_app_user
      #cast pour avoir des 1 ou 0
    , CAST(first_name AS INT64) AS first_name
    , CAST(address AS INT64) AS address
    , CAST(date_moving AS INT64) AS date_moving
    , CAST(email AS INT64) AS email
    , CAST(summary AS INT64) AS summary
    , CAST(page_converted_first_name AS INT64) AS page_converted_first_name
    , CAST(page_converted_address AS INT64) AS page_converted_address
    , CAST(page_converted_date_moving AS INT64) AS page_converted_date_moving
    , CAST(page_converted_email AS INT64) AS page_converted_email
    , CAST(dashboard_zero_valid AS INT64) AS dashboard_zero_valid

    # les pages uniquement du parcours poste si nouvelles pages à rajouter aussi dans ab_pages_poste
    FROM (
      SELECT 
        id_universal AS id_app_user 
      , MAX(IF(name LIKE 'first_name' ,TRUE,FALSE)) AS first_name
      , MAX(IF(name LIKE 'address',TRUE,FALSE)) AS address
      , MAX(IF(name LIKE 'date_moving',TRUE,FALSE)) AS date_moving
      , MAX(IF(name LIKE 'email',TRUE,FALSE)) AS email
      , MAX(name = 'summary') AS summary

      , MAX(name LIKE 'page_converted_first_name',TRUE,FALSE)) AS page_converted_first_name
      , MAX(name LIKE 'page_converted_address' ,TRUE,FALSE)) AS page_converted_address
      , MAX(name LIKE 'page_converted_date_moving',TRUE,FALSE)) AS page_converted_date_moving
      , MAX(name LIKE 'page_converted_email',TRUE,FALSE)) AS page_converted_email
      , MAX(name = 'dashboard_zero_valid') AS dashboard_zero_valid
      , MAX(name LIKE 'page_converted_intro_logic-immo') AS page_converted_intro_logicimmo

      FROM `souscritoo-1343.star_schema.fact_table_tracking_pages`
      GROUP BY id_app_user
      HAVING MAX(url LIKE '%mon-compte/intro%')
    )
  ) USING(id_app_user)
    
  LEFT JOIN ( # device and browser 
    SELECT 
      id_app_user
    , device
    , browser

    FROM (
     SELECT      
        DISTINCT user_id AS id_app_user 
        
      , FIRST_VALUE(
          context_user_agent IGNORE NULLS
        ) OVER (
          PARTITION BY user_id 
          ORDER BY timestamp 
          ROWS BETWEEN UNBOUNDED PRECEDING 
            AND UNBOUNDED FOLLOWING 
        ) AS context_user_agent 

     FROM `souscritoo-1343.prod_app_sct.identifies` 
    )

    LEFT JOIN (
      SELECT 
        context_user_agent
      , ANY_VALUE(browser) AS browser
      , ANY_VALUE(browser_version) AS browser_version
      , ANY_VALUE(os) AS os
      , ANY_VALUE(os_version) AS os_version

      , CASE
          WHEN ANY_VALUE(pc) THEN 'desktop'
          WHEN ANY_VALUE(tablet) THEN 'tablet'
          WHEN ANY_VALUE(mobile) THEN 'mobile'
        END AS device
        
      , ANY_VALUE(device_brand) AS device_brand
      , ANY_VALUE(device_model) AS device_model

      FROM `souscritoo-1343.Views.product_ar_useragent` 

      GROUP BY context_user_agent
    ) USING (context_user_agent)

  ) USING(id_app_user)

  LEFT JOIN ( #attribution bu facon produit 
    SELECT
      id_app_user  
    , source
    , source2
    
    FROM `souscritoo-1343.star_schema.attribution` 
  ) USING (id_app_user)
)
  
SELECT #somme des prospects par jour device on pourrait rajouter les bu style produit 
  date 
, id_text 
, source
, source2
, device
, COUNT(id_app_user) AS app_user
, SUM(first_name) AS first_name
, SUM(first_name_conso) AS first_name_conso
, SUM(address) AS address
, SUM(address_conso) AS address_conso
, SUM(date_moving) AS date_moving
, SUM(date_moving_conso) AS date_moving_conso
, SUM(email) AS email
, SUM(email_conso) AS email_conso
, SUM(summary) AS summary
, SUM(summary_conso) AS summary_conso

FROM table 
GROUP BY date, id_text, source , source2 , device
ORDER BY date DESC