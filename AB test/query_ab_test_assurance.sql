# souscritoo-1343:Views.product_ab_test_assurance_datastudio
#Dashboard : Funnel App Only
WITH 
  table AS (
    SELECT 
      id_app_user 
    , date 
    , source
    , source2
    , id_text 
    , device 
    , IF(is_moving=1,1,page_converted_is_moving) AS is_moving
    , page_converted_is_moving
    , IF(house=1,1,page_converted_house) AS house
    , page_converted_house
    , IF(inhabitants=1,1,page_converted_inhabitants) AS inhabitants
    , page_converted_inhabitants
    , IF(assets_value=1,1,page_converted_assets_value) AS assets_value
    , page_converted_assets_value
    , IF(particularities=1,1,page_converted_particularities) AS particularities
    , page_converted_particularities
    , IF(contract_begin=1,1,page_converted_contract_begin) AS contract_begin
    , page_converted_contract_begin
    , IF(address=1,1,page_converted_address) AS address
    , page_converted_address
    , IF(personal_info=1,1,page_converted_personal_info) AS personal_info
    , page_converted_personal_info
    , IF(offers=1,1,page_converted_offers) AS offers
    , page_converted_offers
    , IF(payment=1,1,page_converted_payment) AS payment
    , page_converted_payment
    , IF(summary=1,1,page_converted_summary) AS summary
    , page_converted_summary
    , CAST(valid AS INT64) AS valid
    , client_e

    # dim_app_user
    FROM (
      SELECT
        CAST(id_app_user AS STRING) AS id_app_user   
      , id_text 
      , DATE(date_joined) AS date 
      
      FROM `souscritoo-1343.star_schema.dimension_app_user`
    )

    # pages
    JOIN (
      SELECT 
        id_app_user 
      , CAST(is_moving AS INT64) AS is_moving
      , CAST(page_converted_is_moving AS INT64) AS page_converted_is_moving
      , CAST(house AS INT64) AS house
      , CAST(page_converted_house AS INT64) AS page_converted_house
      , CAST(inhabitants AS INT64) AS inhabitants
      , CAST(page_converted_inhabitants AS INT64) AS page_converted_inhabitants
      , CAST(assets_value AS INT64) AS assets_value
      , CAST(page_converted_assets_value AS INT64) AS page_converted_assets_value
      , CAST(particularities AS INT64) AS particularities
      , CAST(page_converted_particularities AS INT64) AS page_converted_particularities
      , CAST(contract_begin AS INT64) AS contract_begin
      , CAST(page_converted_contract_begin AS INT64) AS page_converted_contract_begin
      , CAST(address AS INT64) AS address
      , CAST(page_converted_address AS INT64) AS page_converted_address
      , CAST(personal_info AS INT64) AS personal_info
      , CAST(page_converted_personal_info AS INT64) AS page_converted_personal_info
      , CAST(offers AS INT64) AS offers
      , CAST(page_converted_offers AS INT64) AS page_converted_offers
      , CAST(payment AS INT64) AS payment
      , CAST(page_converted_payment AS INT64) AS page_converted_payment
      , CAST(summary AS INT64) AS summary
      , CAST(page_converted_summary AS INT64) AS page_converted_summary
      FROM `souscritoo-1343.Views.product_app_ab_pages_assurance` 
    ) USING(id_app_user)

    # device and browser 
    LEFT JOIN ( 
      SELECT 
        id_app_user
      , device
      , browser

      FROM (
       SELECT      
          DISTINCT user_id AS id_app_user 
          
        , FIRST_VALUE(
            context_user_agent IGNORE NULLS
          ) OVER (
            PARTITION BY user_id 
            ORDER BY timestamp 
            ROWS BETWEEN UNBOUNDED PRECEDING 
              AND UNBOUNDED FOLLOWING 
          ) AS context_user_agent 

       FROM `souscritoo-1343.prod_app_sct.identifies` 
      )

      LEFT JOIN (
        SELECT 
          context_user_agent
        , ANY_VALUE(browser) AS browser
        , ANY_VALUE(browser_version) AS browser_version
        , ANY_VALUE(os) AS os
        , ANY_VALUE(os_version) AS os_version

        , CASE
            WHEN ANY_VALUE(pc) THEN 'desktop'
            WHEN ANY_VALUE(tablet) THEN 'tablet'
            WHEN ANY_VALUE(mobile) THEN 'mobile'
          END AS device
          
        , ANY_VALUE(device_brand) AS device_brand
        , ANY_VALUE(device_model) AS device_model

        FROM `souscritoo-1343.Views.product_ar_useragent` 

        GROUP BY context_user_agent
      ) USING (context_user_agent)
    ) USING(id_app_user)

    # validation et client_e 
    LEFT JOIN (
      SELECT
        CAST(id_app_user AS STRING) AS id_app_user
      , MAX(IF(
          fact_table_app_contract.id_dim_created_date < 20180818
        , valid
        , valid AND origin_contract ='app'
        )) AS valid
      , MAX(
          CASE
            WHEN fact_table_app_contract.id_dim_created_date < 20180818 AND valid
              THEN conversion_probability
            WHEN valid AND origin_contract ='app'
              THEN conversion_probability
            ELSE NULL
          END
        ) AS client_e

      FROM `souscritoo-1343.star_schema.fact_table_app_contract` AS fact_table_app_contract
      LEFT JOIN `souscritoo-1343.star_schema.fact_table_contract`
        USING (id_dim_sf_opportunity)

      WHERE id_dim_app_contract_insurance IS NOT NULL

      GROUP BY id_app_user
    ) USING(id_app_user)

    # BU 
    LEFT JOIN (
      SELECT
        id_app_user  
      , source
      , source2

      FROM `souscritoo-1343.star_schema.attribution` 
    ) USING(id_app_user)
  )
  
SELECT 
  date 
, id_text 
, device 
, source
, source2

, COUNT(id_app_user) AS app_user
, SUM(is_moving) AS is_moving
, SUM(page_converted_is_moving) AS page_converted_is_moving
, SUM(house) AS house
, SUM(page_converted_house) AS page_converted_house
, SUM(inhabitants) AS inhabitants
, SUM(page_converted_inhabitants) AS page_converted_inhabitants
, SUM(assets_value) AS assets_value
, SUM(page_converted_assets_value) AS page_converted_assets_value
, SUM(particularities) AS particularities
, SUM(page_converted_particularities) AS page_converted_particularities
, SUM(contract_begin) AS contract_begin
, SUM(page_converted_contract_begin) AS page_converted_contract_begin
, SUM(address) AS address
, SUM(page_converted_address) AS page_converted_address
, SUM(personal_info) AS personal_info
, SUM(page_converted_personal_info) AS page_converted_personal_info
, SUM(offers) AS offers
, SUM(page_converted_offers) AS page_converted_offers
, SUM(payment) AS payment
, SUM(page_converted_payment) AS page_converted_payment
, SUM(summary) AS summary
, SUM(page_converted_summary) AS page_converted_summary
, SUM(valid) AS valid
, SUM(client_e) AS client_e

FROM table 
WHERE date >= '2018-08-24'
GROUP BY date, id_text, device, source , source2  
ORDER BY date DESC