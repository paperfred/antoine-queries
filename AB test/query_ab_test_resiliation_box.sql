#souscritoo-1343:Views.product_ab_test_cancellation_box
#Dashboard : Funnel App Only
WITH 
  table AS (
    SELECT 
      date 
    , id_text
    , device 
    , id_app_user
    , CAST(provider_date_commitment AS INT64) AS provider_date_commitment
    , CAST(page_converted_provider_date_commitment AS INT64) AS page_converted_provider_date_commitment
    , CAST(address AS INT64) AS address
    , CAST(page_converted_address AS INT64) AS page_converted_address
    , CAST(reference_contract AS INT64) AS reference_contract
    , CAST(page_converted_reference_contract AS INT64) AS page_converted_reference_contract
    , CAST(summary AS INT64) AS summary
    , CAST(page_converted_summary AS INT64) AS page_converted_summary
    , IFNULL(CAST(resiliation_box_valid AS INT64), 0) AS resiliation_box_valid

    # dim_app_user
    FROM (
      SELECT 
        CAST(id_app_user AS STRING) AS id_app_user
      , id_text
      , DATE(date_joined) AS date
      FROM `souscritoo-1343.star_schema.dimension_app_user`
      WHERE DATE(date_joined) > '2018-08-23'
    )

    # pages
    JOIN (
      SELECT
        id_universal AS id_app_user
      , ANY_VALUE(context_user_agent) AS context_user_agent
      , MAX(name = 'provider_date_commitment') AS provider_date_commitment
      , MAX(name = 'page_converted_provider_date_commitment') AS page_converted_provider_date_commitment
      , MAX(name = 'address') AS address
      , MAX(name = 'page_converted_address') AS page_converted_address
      , MAX(name = 'reference_contract') AS reference_contract
      , MAX(name = 'page_converted_reference_contract') AS page_converted_reference_contract
      , MAX(name = 'summary') AS summary
      , MAX(name = 'page_converted_summary') AS page_converted_summary

      FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 

      WHERE url LIKE '%mon-compte/resiliation-box%'

      GROUP BY id_app_user
    ) USING (id_app_user)

    # BU 
    LEFT JOIN (
      SELECT
        id_app_user
      , id_prospect
      , source
      , source2
      
      FROM `souscritoo-1343.star_schema.attribution` 
    ) USING(id_app_user)

    # Validation résiliation
    LEFT JOIN (
      SELECT 
        id_prospect 
      , MAX(status IN (
          'CANCEL_VALID',
          'CANCEL_INQUIRY',
          'CANCEL_ABORT',
          'CANCEL_MAIL_SENT',
          'CANCEL_RECEIPT_RECEIVED'
        )) AS resiliation_box_valid
      FROM `souscritoo-1343.star_schema.fact_table_cancellations` 
      WHERE cancelled_from_app 
        AND recordtype_name = 'Résiliation Box'
      GROUP BY 1
    ) USING (id_prospect)
    
    # device and browser 
    LEFT JOIN (
      SELECT 
        context_user_agent
      , ANY_VALUE(browser) AS browser
      , ANY_VALUE(browser_version) AS browser_version
      , ANY_VALUE(os) AS os
      , ANY_VALUE(os_version) AS os_version

      , CASE
          WHEN ANY_VALUE(pc) THEN 'desktop'
          WHEN ANY_VALUE(tablet) THEN 'tablet'
          WHEN ANY_VALUE(mobile) THEN 'mobile'
        END AS device
        
      , ANY_VALUE(device_brand) AS device_brand
      , ANY_VALUE(device_model) AS device_model

      FROM `souscritoo-1343.Views.product_ar_useragent` 

      GROUP BY context_user_agent
    ) USING (context_user_agent)
  )

SELECT 
  date 
, id_text 
, device 
, COUNT(DISTINCT id_app_user) AS appuser 
,	SUM(provider_date_commitment) AS provider_date_commitment
,	SUM(page_converted_provider_date_commitment) AS page_converted_provider_date_commitment
,	SUM(IF(address!=1,page_converted_address,address)) AS address
,	SUM(page_converted_address) AS page_converted_address
,	SUM(reference_contract) AS reference_contract
,	SUM(page_converted_reference_contract) AS page_converted_reference_contract
,	SUM(summary) AS summary
,	SUM(page_converted_summary) AS page_converted_summary
, SUM(resiliation_box_valid) AS resiliation_box_valid 

FROM table

GROUP BY date, id_text, device