#souscritoo-1343:Views.product_ab_test_mobile_data_studio
#Dashboard : Funnel App Only
WITH 
  table AS (
    SELECT 
      id_app_user 
    , date 
    , IF(valid = TRUE,CAST(IFNULL(valid,FALSE) AS INT64),page_converted_summary) AS valid  
    , id_text 
    , device 
    , source
    , source2
    , client_e
    , IF(offers = 1,1,page_converted_offers) AS offers
    , IF(personal_info = 1,1,page_converted_personal_info) AS personal_info
    , IF(address_cellular = 1,1,page_converted_address_cellular) AS address_cellular  
    , IF(number_cellular = 1,1,page_converted_number_cellular) AS number_cellular
    , IF(payment = 1,1,page_converted_payment) AS payment  
    , IF(payment_sim = 1,1,page_converted_payment_sim) AS payment_sim
    , IF(summary = 1,1,page_converted_summary) AS summary 
    
    , page_converted_offers
    , page_converted_personal_info
    , page_converted_address_cellular
    , page_converted_number_cellular
    , page_converted_payment
    , page_converted_payment_sim
    , page_converted_summary

    # dim_app_user
    FROM (
      SELECT 
        CAST(id_app_user AS STRING) AS id_app_user
      , id_text
      , DATE(date_joined) AS date
      FROM `souscritoo-1343.star_schema.dimension_app_user`
      WHERE DATE(date_joined) > '2018-08-23'
    )

    # pages
    JOIN (
      SELECT
        id_universal AS id_app_user
      , ANY_VALUE(context_user_agent) AS context_user_agent
      , CAST(MAX(name LIKE 'offers') AS INT64) AS offers
      , CAST(MAX(name LIKE 'page_converted_offers') AS INT64) AS page_converted_offers
      , CAST(MAX(name LIKE 'personal_info') AS INT64) AS personal_info
      , CAST(MAX(name LIKE 'page_converted_personal_info') AS INT64) AS page_converted_personal_info
      , CAST(MAX(name LIKE 'address_cellular') AS INT64) AS address_cellular
      , CAST(MAX(name LIKE 'page_converted_address_cellular') AS INT64) AS page_converted_address_cellular
      , CAST(MAX(name LIKE 'number_cellular' ) AS INT64) AS number_cellular 
      , CAST(MAX(name LIKE 'page_converted_number_cellular' ) AS INT64) AS page_converted_number_cellular
      , CAST(MAX(name LIKE 'payment') AS INT64) AS payment
      , CAST(MAX(name LIKE 'page_converted_payment') AS INT64) AS page_converted_payment
      , CAST(MAX(name LIKE 'payment_sim') AS INT64) AS payment_sim
      , CAST(MAX(name LIKE 'page_converted_payment_sim') AS INT64) AS page_converted_payment_sim
      , CAST(MAX(name LIKE 'summary') AS INT64) AS summary
      , CAST(MAX(name LIKE 'page_converted_summary') AS INT64) AS page_converted_summary

      FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 

      WHERE url LIKE '%mon-compte/mobile%'

      GROUP BY id_app_user
    ) USING(id_app_user)

    # device and browser 
    LEFT JOIN (
      SELECT 
        context_user_agent
      , ANY_VALUE(browser) AS browser
      , ANY_VALUE(browser_version) AS browser_version
      , ANY_VALUE(os) AS os
      , ANY_VALUE(os_version) AS os_version

      , CASE
          WHEN ANY_VALUE(pc) THEN 'desktop'
          WHEN ANY_VALUE(tablet) THEN 'tablet'
          WHEN ANY_VALUE(mobile) THEN 'mobile'
        END AS device
        
      , ANY_VALUE(device_brand) AS device_brand
      , ANY_VALUE(device_model) AS device_model

      FROM `souscritoo-1343.Views.product_ar_useragent` 

      GROUP BY context_user_agent
    ) USING (context_user_agent)

    # validation et client_e 
    LEFT JOIN (
      SELECT
        CAST(id_app_user AS STRING) AS id_app_user
      , MAX(IF(
          fact_table_app_contract.id_dim_created_date < 20180818
        , valid
        , valid AND origin_contract ='app'
        )) AS valid
      , MAX(
          CASE
            WHEN fact_table_app_contract.id_dim_created_date < 20180818 AND valid
              THEN conversion_probability
            WHEN valid AND origin_contract ='app'
              THEN conversion_probability
            ELSE NULL
          END
        ) AS client_e

      FROM `souscritoo-1343.star_schema.fact_table_app_contract` AS fact_table_app_contract
      LEFT JOIN `souscritoo-1343.star_schema.fact_table_contract`
        USING (id_dim_sf_opportunity)

      WHERE id_dim_app_contract_cellular IS NOT NULL

      GROUP BY id_app_user
    ) USING(id_app_user)
      
    # attribution
    LEFT JOIN (
      SELECT
        id_app_user  
      , source
      , source2
      
      FROM `souscritoo-1343.star_schema.attribution` 
    ) USING(id_app_user)
  )
  
SELECT 
  date 
, id_text 
, device 
, source
, source2

, COUNT(id_app_user) AS app_user
, SUM(offers) AS offers
, SUM(page_converted_offers) AS page_converted_offers
, SUM(personal_info) AS personal_info
, SUM(page_converted_personal_info) AS page_converted_personal_info
, SUM(address_cellular) AS address_cellular
, SUM(page_converted_address_cellular) AS page_converted_address_cellular
, SUM(number_cellular) AS number_cellular
, SUM(page_converted_number_cellular) AS page_converted_number_cellular
, SUM(payment) AS payment
, SUM(page_converted_payment) AS page_converted_payment
, SUM(payment_sim) AS payment_sim
, SUM(page_converted_payment_sim) AS page_converted_payment_sim
, SUM(summary) AS summary
, SUM(page_converted_summary) AS page_converted_summary
, SUM(valid) AS valid
, SUM(client_e) as client_e
FROM table 
GROUP BY date, id_text, device, source , source2  
ORDER BY date DESC