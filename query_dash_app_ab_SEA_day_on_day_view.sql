#Views : souscritoo-1343:Views.product_app_ab_SEA_day_on_day_view
#Dashboard : App dashboard
WITH 
  table AS 
  (
    SELECT      
      SUM(clicks) AS click 
    , (LOWER(account) LIKE '%app%' OR LOWER(campaign_name) LIKE '%app%') AND NOT campaign_name LIKE r'%\%%'
        OR LOWER(account) LIKE '%fvp%'
        OR campaign_id = '1411828682' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828703' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828706' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828694' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828712' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828697' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828685' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828709' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828559' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828688' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828691' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828700' AND date BETWEEN '2018-05-23' AND  '2018-05-31'
        OR campaign_id = '1411828553' AND date BETWEEN '2018-05-23' AND  '2018-06-04'
        OR campaign_id = '1411828556' AND date BETWEEN '2018-05-23' AND  '2018-06-04'
        AS is_app_bool
    , SUM(impression) AS impression
    , SUM(costs) AS costs
    , date 
    FROM `souscritoo-1343.star_schema.dimension_sea_keywords`
    GROUP BY date, is_app_bool 
  )
,
  click AS
  (
    SELECT 
      date 
    , SUM(IF(is_app_bool = TRUE  ,click,0))  AS gclid_app 
    , SUM(IF(is_app_bool = FALSE ,click,0))  AS gclid_call
    , SUM(IF(is_app_bool = TRUE  , impression ,0))  AS impression_app 
    , SUM(IF(is_app_bool = FALSE , impression ,0))  AS impression_call
    , SUM(IF(is_app_bool = TRUE  , costs ,0))  AS costs_app 
    , SUM(IF(is_app_bool = FALSE , costs ,0))  AS costs_call
    FROM  table
    GROUP BY date
  )
,

  prospect AS 
  (
    SELECT 
      COUNT(IF(adw_is_app = TRUE,id_prospect,NULL)) AS prospect_app
    , COUNT(IF(adw_is_app = FALSE,id_prospect,NULL)) AS prospect_call
    , COUNTIF(IF(adw_is_app= FALSE,is_answeredProspect,NULL)) AS is_answeredProspect 
    , COUNTIF(If(adw_is_app = FALSE,is_presentedProspect,NULL)) AS is_presentedProspect 
    , COUNTIF(IF(adw_is_app = TRUE,is_qualifiedProspect,NULL)) AS is_qualifiedProspect_app
    , COUNTIF(IF(adw_is_app= FALSE,is_qualifiedProspect,NULL)) AS is_qualifiedProspect_call
    , COUNTIF(IF(adw_is_app= TRUE,is_raw_client,NULL)) AS is_raw_client_app 
    , COUNTIF(IF(adw_is_app= FALSE,is_raw_client,NULL)) AS is_raw_client_call 
    , COUNTIF(IF(adw_is_app= TRUE,is_client_net,NULL)) AS is_client_net_app 
    , COUNTIF(IF(adw_is_app= FALSE,is_client_net,NULL)) AS is_client_net_call
    , SUM(IF(adw_is_app= TRUE,is_client,NULL)) AS is_client_app 
    , SUM(IF(adw_is_app= FALSE,is_client,NULL)) AS is_client_call
    , SUM(IF(adw_is_app= TRUE,prob_revenue,NULL)) AS prob_revenue_app 
    , SUM(IF(adw_is_app= FALSE,prob_revenue,NULL)) AS prob_revenue_call  
    , COUNTIF(validated) AS app_validated
    , DATE(joined_at)  AS date 
    FROM `souscritoo-1343.star_schema.prospect_source_v2` 
    LEFT JOIN 
        (
          SELECT
            id_prospect 
          , MAX(IFNULL(contract_status IN ('NET','BRUT'),FALSE)) AS is_raw_client
          , MAX(IFNULL(contract_status ='NET',FALSE)) AS is_client_net
          , MAX(conversion_probability) AS is_client
          , SUM(prob_revenue) AS prob_revenue 
          FROM `souscritoo-1343.star_schema.fact_table_contract` 
          GROUP BY id_prospect 
        )
        USING(id_prospect )
    LEFT JOIN
        (
        SELECT 
          id_prospect 
        , MAX(valid) AS validated
        FROM `souscritoo-1343.star_schema.fact_table_app_contract` 
        GROUP BY id_prospect 
        )
        USING(id_prospect )
        
    WHERE business_unit LIKE 'PPC' AND (source_lvl2 = 'google' OR source_lvl2 IS NULL)
    
    GROUP BY date
  )
  
SELECT *

, SAFE_DIVIDE(prospect_app,gclid_app) AS app_p_c
, SAFE_DIVIDE(prospect_call,gclid_call) AS call_p_c
, SAFE_DIVIDE(gclid_app,impression_app) AS CTR_app
, SAFE_DIVIDE(gclid_call,impression_call) AS CTR_call
, SAFE_DIVIDE(prob_revenue_app,is_client_app) AS revenue_contract_app
, SAFE_DIVIDE(prob_revenue_call,is_client_call) AS revenue_contract_call
, SAFE_DIVIDE(costs_app,gclid_app) AS CPC_app
, SAFE_DIVIDE(costs_call,gclid_call) AS CPC_call
, SAFE_DIVIDE(is_client_app,prospect_app) AS transfo_app
, SAFE_DIVIDE(is_client_call ,prospect_call ) AS transfo_call
, SAFE_DIVIDE(prob_revenue_app,prospect_app) AS ARPU_app
, SAFE_DIVIDE(prob_revenue_call,prospect_call ) AS ARPU_call
, SAFE_DIVIDE(is_client_net_app,is_raw_client_app) AS ratio_client_app
, SAFE_DIVIDE(is_client_net_call,is_raw_client_call ) AS ratio_client_call
FROM click 
JOIN prospect 
  USING(date) 
ORDER BY date DESC