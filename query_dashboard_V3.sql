#standardSQL
#souscritoo-1343:souscritoo_bi.product_dashboard_v3 layer : 7
#Views.product_app_ab_dash_de_dash
#Dashboard : Dashboard v3
WITH table AS (
  SELECT 
    id_app_user
  , app_or_call_user   
  , id_text
  , is_moving
  , date_joined
  , DATE_TRUNC(date_joined, MONTH) AS month
  , last_login
  , source
  , source2
  , segment
  , IFNULL(IFNULL(sum_cancellations,0) + IFNULL(valid_box_app,0) + IFNULL(valid_energy_app,0) + IFNULL(valid_cellular_app,0) + IFNULL(valid_poste_app,0) + IFNULL(valid_insurance_app,0),0) AS services_termines 

  # services débutés et terminés depuis le dash 
  , IFNULL( IF(starting_box_from_dash=1,valid_box_app,0) ,0) AS valid_box_app
  , IFNULL( IF(starting_energy_from_dash=1,valid_energy_app,0) ,0) AS valid_energy_app
  , IFNULL( IF(starting_assurance_from_dash=1,valid_insurance_app,0) ,0) AS valid_insurance_app
  , IFNULL( IF(starting_courrier_from_dash=1,valid_poste_app,0) ,0) AS valid_poste_app
  , IFNULL( IF(starting_mobile_from_dash=1,valid_cellular_app,0) ,0) AS valid_cellular_app
  #  valid peu importe ou 
  
  , valid_box_app = 1 AS valid_box
  , valid_energy_app = 1 AS valid_energy
  , valid_insurance_app = 1 AS valid_insurance
  , valid_poste_app = 1 AS valid_poste
  , valid_cellular_app = 1 AS valid_cellular
  
  # valid call et synchro 
  , IFNULL( valid_box_call ,0) AS valid_box_call
  , IFNULL( valid_energy_call ,0) AS valid_energy_call
  , IFNULL( valid_insurance_call ,0) AS valid_insurance_call
  , IFNULL( valid_poste_call ,0) AS valid_poste_call
  , IFNULL( valid_cellular_call ,0) AS valid_cellular_call
  
  # resiliation 100% app déjà ready pour l assurance et le mobile 
  , IFNULL( resiliation_assurance_valid ,0) AS resiliation_assurance_valid
  , IFNULL( IF(starting_resiliation_box_from_dash=1,resiliation_box_valid,0) ,0) AS resiliation_box_valid
  , IFNULL( IF(starting_resiliation_energy_from_dash=1,resiliation_energie_valid,0) ,0) AS resiliation_energie_valid
  , IFNULL( resiliation_mobile_valid ,0) AS resiliation_mobile_valid
  
  # ouverture de la fenertre du dash (click button) 
  , IFNULL(energy_ouverture,0) AS energy_ouverture
  , IFNULL(box_ouverture,0) AS box_ouverture
  , IFNULL(insurance_ouverture,0) AS insurance_ouverture
  , IFNULL(financement_ouverture,0) AS financement_ouverture
  , IFNULL(poste_ouverture,0) AS poste_ouverture
  , IFNULL(etat_des_lieux_ouverture,0) AS etat_des_lieux_ouverture
  , IFNULL(kiwatch_ouverture,0) AS kiwatch_ouverture
  , IFNULL(carte_gris_ouverture,0) AS carte_gris_ouverture
  , IFNULL(auto_insurance_ouverture,0) AS auto_insurance_ouverture
  , IFNULL(mobile_ouverture,0) AS mobile_ouverture
  , IFNULL(car_rental_ouverture,0) AS car_rental_ouverture 
  , IFNULL(pretto_ouverture,0) as pretto_ouverture
  
  # debut depuis le dash (click button)
  , IFNULL(starting_energy_from_dash,0) AS starting_energy_from_dash
  , IFNULL(starting_box_from_dash,0) AS starting_box_from_dash
  , IFNULL(starting_assurance_from_dash,0) AS starting_assurance_from_dash 
  , IFNULL(starting_younited_credit_from_dash,0) AS starting_younited_credit_from_dash
  , IFNULL(starting_courrier_from_dash,0) AS starting_courrier_from_dash
  , IFNULL(starting_declarer_nouvelle_address_from_dash,0) AS starting_declarer_nouvelle_address_from_dash
  , IFNULL(starting_etat_des_lieux_from_dash,0) AS starting_etat_des_lieux_from_dash 
  , IFNULL(starting_alarme_from_dash,0) AS starting_alarme_from_dash
  , IFNULL(starting_carte_grise_from_dash,0) AS starting_carte_grise_from_dash
  , IFNULL(starting_resiliation_energy_from_dash,0) AS starting_resiliation_energy_from_dash
  , IFNULL(starting_resiliation_box_from_dash,0) AS starting_resiliation_box_from_dash
  , IFNULL(starting_mobile_from_dash,0) AS starting_mobile_from_dash
  , IFNULL(starting_direct_assu_auto,0) AS starting_direct_assu_auto
  , IFNULL(starting_mb_auto,0) AS starting_mb_auto
  , IFNULL(starting_pretto_from_dash,0) as starting_pretto_from_dash
  , IFNULL(starting_europcar_from_dash,0) AS starting_europcar_from_dash
  , IFNULL(starting_lesfurets_from_dash,0) AS starting_lesfurets_from_dash
  , IFNULL(starting_rentacar_from_dash,0) AS starting_rentacar_from_dash
  # a vu ou non le dash 
  , IFNULL(is_dashbord,0) AS is_dashbord #dashoArd 
  
  FROM ( # dim_app_user 
    SELECT
      CAST(id_app_user AS STRING) AS id_app_user   
    , id_text 
    , is_moving 
    , DATE(date_joined) AS date_joined 
    , DATE(IFNULL(last_login ,date_joined)) AS last_login 
    , origin_user AS app_or_call_user 
    FROM `souscritoo-1343.star_schema.dimension_app_user`
  )
    
  LEFT JOIN ( #valid 
    SELECT 
     CAST(id_app_user AS STRING ) AS id_app_user 
     #valid app
    ,  MAX(CAST(id_dim_app_contract_box IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'app') AS INT64)) AS valid_box_app
    ,  MAX(CAST(id_dim_app_contract_energy IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'app') AS INT64))  AS valid_energy_app
    ,  MAX(CAST(id_dim_app_contract_insurance IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'app') AS INT64)) AS valid_insurance_app
    ,  MAX(CAST(id_dim_app_contract_poste IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'app') AS INT64))  AS valid_poste_app
    ,  MAX(CAST(id_dim_app_contract_cellular IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'app') AS INT64)) AS valid_cellular_app
      #valid call
    ,  MAX(CAST(id_dim_app_contract_box IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'call') AS INT64))  AS valid_box_call
    ,  MAX(CAST(id_dim_app_contract_energy IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'call') AS INT64))  AS valid_energy_call
    ,  MAX(CAST(id_dim_app_contract_insurance IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'call') AS INT64)) AS valid_insurance_call
    ,  MAX(CAST(id_dim_app_contract_poste IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'call') AS INT64))  AS valid_poste_call
    ,  MAX(CAST(id_dim_app_contract_cellular IS NOT NULL AND IF(id_dim_created_date < 20180819,valid, valid AND origin_contract = 'call') AS INT64))  AS valid_cellular_call
    FROM  `souscritoo-1343.star_schema.fact_table_app_contract`  # a partir du 19/08 origin = app/call 
    GROUP BY 1 
    ORDER BY id_app_user 
  ) USING(id_app_user)

  LEFT JOIN ( #attribution bu facon produit 
    SELECT
      id_app_user 
    , id_prospect   
    , source
    , source2
    , segment
    FROM `souscritoo-1343.star_schema.attribution` 
  ) USING(id_app_user)

  LEFT JOIN ( # actions commencées depuis le dash 
    SELECT 
      id_universal AS id_app_user 
     # ouverture des blocks
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_energy%' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS energy_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_box%' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS box_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_insurance%' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS insurance_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_organise_move%' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS financement_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_poste%' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS poste_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_state_fictures%' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS etat_des_lieux_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_alarm%' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS kiwatch_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_vehicle_documentation%' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS carte_gris_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_motor_insur%' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS auto_insurance_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_cellular_subscription' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS mobile_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_housing_loan' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS pretto_ouverture
    , CAST(MAX(IFNULL(id_click LIKE '_bloc_rent_a_car' AND location = 'dropdown-content' AND value = 'open',FALSE)) AS INT64) AS car_rental_ouverture
    # debut des parcours 
    , CAST(MAX(IFNULL(id_click LIKE 'internal_link' AND REGEXP_CONTAINS(value,r'energie'),FALSE)) AS INT64) AS starting_energy_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'internal_link' AND REGEXP_CONTAINS(value,r'resiliation-energie'),FALSE)) AS INT64) AS starting_resiliation_energy_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'internal_link' AND REGEXP_CONTAINS(value,r'box'),FALSE)) AS INT64) AS starting_box_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'internal_link' AND REGEXP_CONTAINS(value,r'resiliation-box'),FALSE)) AS INT64) AS starting_resiliation_box_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'internal_link' AND REGEXP_CONTAINS(value,r'assurance'),FALSE)) AS INT64) AS starting_assurance_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'internal_link' AND REGEXP_CONTAINS(value,r'courrier'),FALSE)) AS INT64) AS starting_courrier_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'internal_link' AND REGEXP_CONTAINS(value,r'mobile'),FALSE)) AS INT64) AS starting_mobile_from_dash

    , CAST(MAX(IFNULL(id_click LIKE 'external_link' AND value = 'https://psl.service-public.fr/mademarche/JeChangeDeCoordonnees/demarche',FALSE)) AS INT64) AS starting_declarer_nouvelle_address_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'external_link' AND value = 'https://www.papernest.com/etat-des-lieux/',FALSE)) AS INT64) AS starting_etat_des_lieux_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'external_link' AND REGEXP_CONTAINS(value,r'kiwatch'),FALSE)) AS INT64) AS starting_alarme_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'external_link' AND value = 'https://www.service-public.fr/particuliers/vosdroits/F12118',FALSE)) AS INT64) AS starting_carte_grise_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'external_link' AND value LIKE 'https://tracking.publicidees.com/clic.php?promoid=160640&progid=1900&partid=46287',FALSE)) AS INT64) AS starting_direct_assu_auto   
    , CAST(MAX(IFNULL(id_click LIKE 'external_link' AND value LIKE 'https://tracking.publicidees.com/clic.php?promoid=170777&progid=5775&partid=46287',FALSE)) AS INT64) AS starting_mb_auto
    , CAST(MAX(IFNULL(id_click LIKE 'external_link' AND value LIKE 'https://papernest.app.pretto.fr/project/choice%',FALSE)) AS INT64) AS starting_pretto_from_dash  
    , CAST(MAX(IFNULL(id_click LIKE 'external_link' AND value = 'http://www.awin1.com/cread.php?awinaffid=291633&awinmid=7418&clickref=app&p=https%3A%2F%2Fwww.europcar.fr%2Flocation-utilitaire-et-camion', FALSE)) AS INT64) AS starting_europcar_from_dash
    , CAST(MAX(IFNULL(id_click LIKE 'external_link' AND value = 'https://partenariats.lesfurets.com/papernest/assurance-auto#voiture', FALSE)) AS INT64) AS starting_lesfurets_from_dash
    , CAST(MAX(IFNULL(
        id_click LIKE 'external_link' AND value = 'http://www.awin1.com/awclick.php?gid=309603&mid=7059&awinaffid=291633&linkid=628998&clickref=' AND id_dim_date < 20181206
        OR id_click LIKE 'external_link' AND value = 'http://www.awin1.com/awclick.php?awinaffid=291633&awinmid=7059&clickref=app' AND id_dim_date >= 20181206
      , FALSE)) AS INT64) AS starting_younited_credit_from_dash
    , CAST(MAX(IFNULL(
        id_click LIKE 'external_link' AND value = 'https://www.awin1.com/cread.php?s=2067487&v=11216&q=325737&r=291633' AND id_dim_date < 20181206
        OR id_click LIKE 'external_link' AND value = 'http://www.awin1.com/cread.php?awinaffid=291633&awinmid=11216&clickref=app&p=https%3A%2F%2Fwww.rentacar.fr%2Fvehicules-utilitaires%3Fawc%3D11216_1544021576_af841866529b87f8f0d21941f390c6c6%23ectrans%3D1' AND id_dim_date >= 20181206
      , FALSE)) AS INT64) AS starting_rentacar_from_dash

    FROM `souscritoo-1343.star_schema.fact_table_tracking_clicked_button` 
    GROUP BY id_universal 
  ) USING(id_app_user)   

  LEFT JOIN ( # is_dashboard 
    SELECT 
      id_universal AS id_app_user
    , CAST(MAX(IFNULL(REGEXP_CONTAINS(name,r'^dashboard_'),FALSE)) AS INT64) AS is_dashbord #event pages quand tu arrives sur le dash 
    FROM `souscritoo-1343.star_schema.fact_table_tracking_pages` 
    GROUP BY id_app_user
  ) USING(id_app_user)
   
   LEFT JOIN ( #cancellation 
    SELECT 
      id_prospect   
    , MAX(IF(REGEXP_CONTAINS(recordtype_name,'MRH') AND status = 'CANCEL_VALID',1,0)) AS resiliation_assurance_valid
    , MAX(IF(REGEXP_CONTAINS(recordtype_name,'Box') AND status = 'CANCEL_VALID',1,0)) AS resiliation_box_valid
    , MAX(IF(REGEXP_CONTAINS(recordtype_name,'Energie') AND status = 'CANCEL_VALID',1,0)) AS resiliation_energie_valid
    , MAX(IF(REGEXP_CONTAINS(recordtype_name,'Mobile') AND status = 'CANCEL_VALID',1,0)) AS resiliation_mobile_valid
    , SUM(CAST(status = 'CANCEL_VALID' AS INT64)) AS sum_cancellations 
    FROM `souscritoo-1343.star_schema.fact_table_cancellations` 
    WHERE cancelled_from_app #true = app
    GROUP BY 1
    ORDER BY id_prospect 
  ) USING(id_prospect)
   
  WHERE date_joined >= '2018-07-30' #date new dash 
)
  
SELECT *
, starting_alarme_from_dash +starting_assurance_from_dash 
+starting_box_from_dash +starting_carte_grise_from_dash 
+starting_courrier_from_dash +starting_declarer_nouvelle_address_from_dash 
+starting_energy_from_dash +starting_etat_des_lieux_from_dash 
+starting_younited_credit_from_dash +starting_direct_assu_auto+starting_pretto_from_dash
+starting_mb_auto+starting_europcar_from_dash+starting_lesfurets_from_dash+starting_rentacar_from_dash
AS parcours_debute #depuis le dash 

FROM table
LEFT JOIN `souscritoo-1343.Views.product_app_ab_device` # pour avoir le device ! 
  USING(id_app_user)
ORDER BY id_app_user  
